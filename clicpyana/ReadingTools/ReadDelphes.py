from clicpyana.AnalysisTools.HzqqEvent import HzqqEvent
from clicpyana.AnalysisTools.TTHEvent import TTHEvent
from clicpyana.AnalysisTools.WWEvent import WWEvent
from clicpyana.AnalysisTools.HmumuEvent import HmumuEvent
import ROOT, sys
from clicpyana.ReadingTools import ReaderTools
import json
from math import atan, sin, exp



class ReadDelphes:
    """reads in the delphes event and fills a HZqqEvent object"""

    def createInputChain(self,delphesOutputTopdir):
        chain = ROOT.TChain("Delphes")
        infileslist = ReaderTools.walkInputDir(delphesOutputTopdir,"root")
        for i in infileslist:
            chain.Add( i )
        return chain

    def getBranchesForHZqq(self,delphesOutputTopdir):
        chain = self.createInputChain(delphesOutputTopdir)
        
        treereader = ROOT.ExRootTreeReader(chain)
        branchdict = {}
        branchdict["VLCjetR07N2"] = treereader.UseBranch("VLCjetR07N2")
        branchdict["JER_VLCjetR07N2"] = treereader.UseBranch("JER_VLCjetR07N2")
        branchdict["JES_VLCjetR07N2"] = treereader.UseBranch("JES_VLCjetR07N2")
        branchdict["JES_VLCjetR10N6"] = treereader.UseBranch("JES_VLCjetR10N6")
        branchdict["VLCjetR07N3"] = treereader.UseBranch("VLCjetR07N3")
        branchdict["VLCjetR07N4"] = treereader.UseBranch("VLCjetR07N4")
        branchdict["JES_VLCjetR07N3"] = treereader.UseBranch("JES_VLCjetR07N3")
        branchdict["JES_VLCjetR07N4"] = treereader.UseBranch("JES_VLCjetR07N4")
        branchdict["VLCjetR10N3"] = treereader.UseBranch("VLCjetR10N3")
        branchdict["VLCjetR10N4"] =  treereader.UseBranch("VLCjetR10N4")
        branchdict["VLCjetR10N5"] =  treereader.UseBranch("VLCjetR10N5")
        branchdict["VLCjetR10N6"] =  treereader.UseBranch("VLCjetR10N6")
        branchdict["truthparticles"] =  treereader.UseBranch("Particle")
        branchdict["mu"] = treereader.UseBranch("Muon")
        branchdict["el"] = treereader.UseBranch("Electron")
        branchdict["ph"] = treereader.UseBranch("Photon")
        self.nentries = treereader.GetEntries()
        self.treeReader = treereader
        return branchdict

    def getBranchesForGeneralAnalysis(self,delphesOutputTopdir):
        chain = self.createInputChain(delphesOutputTopdir)
        treereader = ROOT.ExRootTreeReader(chain)
        branchdict = {}
        branchdict["jet"] =  treereader.UseBranch("VLCjetR10N4")
        branchdict["el"] =treereader.UseBranch("Electron")
        branchdict["mu"] = treereader.UseBranch("Muon")
        branchdict["ph"] = treereader.UseBranch("Photon")
        branchdict["truthparticles"] =  treereader.UseBranch("Particle")
        self.nentries = treereader.GetEntries()
        self.treeReader = treereader
        return branchdict

        
    def __init__(self,configFile):
        """initialize"""
        
        if str(configFile).endswith("json"):
            configDict = open(configFile).read()
            config = json.loads(configDict)
        else:
            config = configFile
            
        delphesOutputTopdir= config["InputTopDir"]
        print("Reading Delphes ROOT files from {}".format(delphesOutputTopdir))
        Analysis = config["Analysis"]
        self.lastEvent = config["Nevents"]
        delphesDir = config["delphesDir"]
        loading = ROOT.gSystem.Load(delphesDir+"libDelphes")
        if not loading == 0 or loading == 1:
            print("TSystem::Load has returned {}".format(loading))
            print("Probably could not find libDelphes. Put it in PATH or execute this in the delphes working directory")
            print("e.g.  /home/schnooru/CLIC/Projects/Delphes/Delphes_VLC/delphes")
            sys.exit()
        try:
            ROOT.gInterpreter.Declare('#include "'+delphesDir+'classes/DelphesClasses.h"')
            ROOT.gInterpreter.Declare('#include "'+delphesDir+'external/ExRootAnalysis/ExRootTreeReader.h"')
        except AttributeError:
            print ("Problem: was not able to find the necessary Delphes headers.")
            pass

        self.nentries = 0
        
        self.branchDict = self.getBranchesForHZqq(delphesOutputTopdir )

        #self.chain = self.getBranchesForHZqq()
        #brVLCjetR10N3s,brVLCjetR10N4s,brVLCjetR10N5s, brEls, brMus = self.getBranchesForHZqq(delphesOutputTopdir )
        #then construct the necessary objects for HZqq events

        #self.readerLoop = range(0,self.nentries)
        #self.chain = ROOT.TChain("Delphes")
        #self.addInputToChain()

        return

    def readerLoop(self):
        return range(0,self.nentries)

    def FillDRjetbcList(self,event):
        drlistb,drlistc =[],[]
        for p in self.branchDict["truthparticles"]:
            if abs(p.PID) == 5: #it's a b
                b = ROOT.TLorentzVector()
                b.SetPtEtaPhiM(p.PT, p.Eta, p.Phi, p.Mass  )
                for j in self.branchDict["VLCjetR10N4"]:
                    jet = ROOT.TLorentzVector()
                    jet.SetPtEtaPhiM(j.PT, j.Eta, j.Phi, j.Mass)
                    drlistb.append( jet.DeltaR(b) )
            elif abs(p.PID) == 4: #it's a c
                c = ROOT.TLorentzVector()
                c.SetPtEtaPhiM(p.PT, p.Eta, p.Phi, p.Mass  )
                for j in self.branchDict["VLCjetR10N4"]:
                    jet = ROOT.TLorentzVector()
                    jet.SetPtEtaPhiM(j.PT, j.Eta, j.Phi, j.Mass)
                    drlistc.append( jet.DeltaR(c) )
        return drlistb, drlistc


    def FillJetsList(self,  njets , name):

        try:
            self.branchDict[name]
        except KeyError:
            print("ERROR: {} has not been added to the branchDict dictionary (in ReadDelphes.getBranchesForHZqq). Exiting.".format(name))
            sys.exit()
        jetlist = []
        for j in xrange(njets):
            jet = ROOT.TLorentzVector()
            #in case N jets were not found, the object is not filled, such that
            #calling its properties fails. Catch this by continuing when there
            #is an attribute error
            try:
                self.branchDict[name].At(j).PT
            except AttributeError:
                continue
            except ReferenceError as e:
                print("Branch seems not to exist. {}".format(e))
            jet.SetPtEtaPhiM(self.branchDict[name].At(j).PT ,self.branchDict[name].At(j).Eta,self.branchDict[name].At(j).Phi,self.branchDict[name].At(j).Mass )
            jetlist.append(jet)
        return jetlist


    def FillParticlesList(self, name):
        particleList = []
        for p in xrange( self.branchDict[name].GetEntries()):
            particle = ROOT.TLorentzVector()
            threemom = ROOT.TVector3()
            threemom.SetPtEtaPhi( self.branchDict[name].At(p).PT ,self.branchDict[name].At(p).Eta,self.branchDict[name].At(p).Phi)
            
            theta = threemom.Theta()
            #print theta, atan( 2*exp(- self.branchDict[name].At(p).Eta ) )
            P = self.branchDict[name].At(p).PT/sin(theta)
            #print P, threemom.Mag()
            particle.SetPtEtaPhiE( self.branchDict[name].At(p).PT ,self.branchDict[name].At(p).Eta,self.branchDict[name].At(p).Phi,P  )
            particleList.append(particle)
        return particleList


    def FillHdecayproducts(self,event):
        for p in self.branchDict["truthparticles"]:
            if p.PID == 25:
                DaughterPID = self.branchDict["truthparticles"][p.D1].PID
                if not DaughterPID == 25: 
                    #print("Found Higgs daughter with PID {}".format(DaughterPID))
                    return [-1*DaughterPID, DaughterPID]
#            if abs(p.PID) == 11 and p.Status ==1:
#                e = ROOT.TLorentzVector()
#                e.SetPtEtaPhiM(p.PT, p.Eta, p.Phi, p.Mass  )  
#                self.HzqqEvent.truth_electronslist.append(e)
#            elif abs(p.PID) == 13 and p.Status ==1:
#                m = ROOT.TLorentzVector()
#                m.SetPtEtaPhiM(p.PT, p.Eta, p.Phi, p.Mass  )  
#                self.HzqqEvent.truth_muonslist.append(m)
        return [0,0]

    def FillTruthLeptons(self,event):
        for p in self.branchDict["truthparticles"]:
            if p.Status == 1 and abs(p.PID) == 11:
                e = ROOT.TLorentzVector()
                e.SetPtEtaPhiM(p.PT, p.Eta, p.Phi, p.Mass  )  
                self.HzqqEvent.truth_electronslist.append(e)
            elif p.Status == 1 and abs(p.PID) == 13:
                m = ROOT.TLorentzVector()
                m.SetPtEtaPhiM(p.PT, p.Eta, p.Phi, p.Mass  )  
                self.HzqqEvent.truth_muonslist.append(m)
        return 

    def FillTruthMuons(self,event):
        truth_muonslist = []
        for p in self.branchDict["truthparticles"]:
            if p.Status == 1 and abs(p.PID) == 13:
                m = ROOT.TLorentzVector()
                m.SetPtEtaPhiM(p.PT, p.Eta, p.Phi, p.Mass  )  
                truth_muonslist.append(m)
        return truth_muonslist
                
    def FillTruthElectrons(self,event):
        truth_electronslist = []
        for p in self.branchDict["truthparticles"]:
            if p.Status == 1 and abs(p.PID) == 11:
                m = ROOT.TLorentzVector()
                m.SetPtEtaPhiM(p.PT, p.Eta, p.Phi, p.Mass  )  
                truth_electronslist.append(m)
        return truth_electronslist
                



    def FillHdecayproducts_massbased(self,event):
        truth_muonslist = []
        truth_blist = []
        for p in self.branchDict["truthparticles"]:
            if abs(p.PID) == 13 and p.Status ==1:
                mu = ROOT.TLorentzVector()
                mu.SetPtEtaPhiM(p.PT, p.Eta, p.Phi, p.Mass  )
                truth_muonslist.append( [mu,p.PID, p.Status] )
            elif abs(p.PID) == 5 and p.Status ==2: #b quarks still decay
                b = ROOT.TLorentzVector()
                b.SetPtEtaPhiM(p.PT, p.Eta, p.Phi, p.Mass  )
                truth_blist.append( [b, p.PID, p.Status] )

        Hdecay_mu = self.TruthDiObjectMass( truth_muonslist, 13 )
        if Hdecay_mu == [-13,13]:
            return Hdecay_mu
        Hdecay_b =self.TruthDiObjectMass( truth_blist, 5 )
        if Hdecay_b == [-5,5]:
            return Hdecay_b
        return [0,0]


    def TruthDiObjectMass(self,objectlist,PDGID):
        for m1 in xrange(len(objectlist)):
            for m2 in xrange(len(objectlist)):
                if m1 <= m2:
                    continue
                po1 = objectlist[m1]
                po2 = objectlist[m2]
                if po1[1]+po2[1] ==0:
                    if abs((po1[0]+po2[0]).M() - 126) < 0.1:
                        #print("\nclose to Higgs, Pt1 = {}, Pt2 ={}\n".format(  po1[0].Pt(), po2[0].Pt(), (po1[0]+po2[0]).M()  ))
                        self.HzqqEvent.mtruthmumu = (po1[0]+po2[0]).M()
                        return [-1* PDGID,PDGID]
        return [0,0]

    def IsWjetsEvent(self,branch):
        #check if there is a W+W- pair or only one sign of W
        #problem: many events from that sample do not have the
        #intermediate W stored in the Delphes branch: the info
        #seems lost in most events completely
        pdgidsOfWcandidates=[]
        #for p in branch:
        #    print p.PID, 
        #    #if p.PID == 25: print  self.branchDict["truthparticles"][p.D1].PID,
        #    #PDGID=p.PID
        #    #if abs(PDGID)==24:
        #    #    pdgidsOfWcandidates.append(PDGID)
        #    print
        return pdgidsOfWcandidates


    def FillGeneralEvent(self,AnalysisEvent,event):
        self.treeReader.ReadEntry(event)
        AnalysisEvent.muonslist = self.FillParticlesList("mu")
        AnalysisEvent.electronslist = self.FillParticlesList("el")
        AnalysisEvent.photonslist = self.FillParticlesList("ph")
        

        return AnalysisEvent

        
    def FillWWEvent(self,event):

        self.WWEvent = WWEvent()
        
        self.WWEvent =self.FillGeneralEvent(self.WWEvent,event)


        #access the number of electrons, muons, and total leptons numbers
        
        
        self.WWEvent.jetsList2 = self.FillJetsList(2,"JER_VLCjetR07N2")
        #self.WWEvent.jetsList2 = self.FillJetsList(2,"VLCjetR07N2")

        self.WWEvent.dijetsmasslist = ReaderTools.DiParticleMasslist (self.WWEvent.jetsList2)
        #self.WWEvent.jetsList3 = self.FillJetsList(3,"VLCjetR07N3")
        #self.WWEvent.jetsList4 = self.FillJetsList(4,"VLCjetR07N4")
        #self.WWEvent.y23 = self.branchDict["VLCjetR07N3"].At(0).ExclYmerge23
        #self.WWEvent.y34 = self.branchDict["VLCjetR07N3"].At(0).ExclYmerge34
        self.WWEvent.IsWjetsEvent= self.IsWjetsEvent(self.branchDict["truthparticles"])

        self.WWEvent.truth_muonslist = self.FillTruthMuons(event)
        self.WWEvent.truth_electronslist = self.FillTruthElectrons(event)


        return self.WWEvent



    def FillTTHEvent(self,event):
        self.TTHEvent = TTHEvent()
        
        #print("This is where TTHEvent should be filled")
        self.TTHEvent = self.FillGeneralEvent(self.TTHEvent,event)
        #print("self.TTHEvent: {} and len of muonslist {}".format(type(self.WWEvent), len(self.WWEvent.electronslist) ))

        self.TTHEvent.jetsList6 = self.FillJetsList(6,"JES_VLCjetR10N6")
        #self.TTHEvent.jetsList6 = self.FillJetsList(6,"VLCjetR10N6")

        
        self.TTHEvent.dijetsmasslist = ReaderTools.DiParticleMasslist (self.TTHEvent.jetsList6)
        #print(self.branchDict["VLCjetR10N3"].At(0).ExclYmerge23  )
        self.TTHEvent.y23 = self.branchDict["VLCjetR10N3"].At(0).ExclYmerge23
        self.TTHEvent.y34 = self.branchDict["VLCjetR10N3"].At(0).ExclYmerge34
        #print("self.TTHEvent: {} and its y23 {}".format(type(self.TTHEvent), self.TTHEvent.y23) )

        #print("next event")
        #for j in self.branchDict["VLCjetR10N6"]:
        #    print j.BTag & ( 1 << 2 ),
        #    #print ( 1 << 0 ),
        self.TTHEvent.nbtag =0
        for j in self.branchDict["VLCjetR10N6"]:
            #if j.BTag & (1<<0): # tight b-tagging (50%WP)
            if j.BTag & (1<<1): # medium b-tagging (70%WP)
            #if j.BTag >=4: #loose b-tagging(90%WP)
                self.TTHEvent.nbtag +=1
        #print self.TTHEvent.nbtag

            
        return self.TTHEvent
    

    def FillHmumuEvent(self,event):
        self.HmumuEvent = HmumuEvent()
        self.HmumuEvent = self.FillGeneralEvent(self.HmumuEvent, event)
        self.HmumuEvent.dimuonmasslist = ReaderTools.DiParticleMasslist( self.HmumuEvent.muonslist )
        return self.HmumuEvent


    def FillHzqqEvent(self, event):

        self.HzqqEvent = HzqqEvent()
        #print(self.branchDict["VLCjetR10N3"].At(0).PT)
        self.HzqqEvent =self.FillGeneralEvent(self.HzqqEvent,event)
        
        self.HzqqEvent.jetsList3 = self.FillJetsList(3,"VLCjetR10N3")
        self.HzqqEvent.jetsList4 = self.FillJetsList(4,"VLCjetR10N4")
        self.HzqqEvent.jetsList5 = self.FillJetsList(5,"VLCjetR10N5")
        if not self.HzqqEvent.FindZcandidateJets( [self.HzqqEvent.jetsList3,self.HzqqEvent.jetsList4, self.HzqqEvent.jetsList5  ]  ):
            print("did not find any useful Z candidate in the event")
        self.HzqqEvent.Recoil()
        self.HzqqEvent.y23 = self.branchDict["VLCjetR10N3"].At(0).ExclYmerge23
        self.HzqqEvent.y34 = self.branchDict["VLCjetR10N3"].At(0).ExclYmerge34

        self.HzqqEvent.dijetsmasslist = ReaderTools.DiParticleMasslist (self.HzqqEvent.jetsList2)
        self.HzqqEvent.dimuonmasslist = ReaderTools.DiParticleMasslist( self.HzqqEvent.muonslist )

        #self.FillTruthLeptons(event)

        #####self.HzqqEvent.Hdecay = self.FillHdecayproducts_massbased(event)
        self.HzqqEvent.Hdecay = self.FillHdecayproducts(event)

        #self.HzqqEvent.dRjetbList,self.HzqqEvent.dRjetcList = self.FillDRjetbcList(event)
        return self.HzqqEvent

#where does it read from? set the input directory in a config file
        
