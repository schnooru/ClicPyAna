import ROOT as r
from clicpyana.AnalysisTools.HHEvent import HHEvent
from math import log, cos


class ReadHHtmvafile:
    """reads the tmva file from Rosa"""


    def  __init__(self,AllHistos, pathToFile, cutsDict,TreeName="TestTree",Has_BDT3=False ):
    #def  __init__(self, Histos,pathToFile, cutsConfig,dsid, BDT_cut=-999,TreeName="TestTree"):
        pathFileToRead = pathToFile
        self.HHEvent = HHEvent()

        ntupfi = r.TFile.Open(pathFileToRead)
        print ("name of ntuple file {}".format(ntupfi.GetName()))

        self.readTree = ntupfi.Get(str(TreeName))
        self.nentries = self.readTree.GetEntries()
        
        print("Running over all events in TTree {}".format(str(TreeName)))
        print("(Total number of events in this tree for all prodIDs {})".format(self.nentries))
        #print("All branches {}".format(self.readTree.GetListOfLeaves().Print()))
        BDT_min = cutsDict["BDT_min"]
        sumbtag_min = cutsDict["sumbtag_min"]
        y34_min = cutsDict["y34_min"]

        nevt = 0
        for event in self.readTree:
            nevt+=1
            if (nevt%100000) == 0:
                print("Event number {}".format(nevt))
            #if not self.passCuts(cutsConfig,event):not yet implemented!!
            #    continue
            ### applying BDT_cut in case BDT3 exists
            ### (cut steered by config):
            if Has_BDT3 and not self.readTree.BDT3 >= BDT_min:
                continue
            if not self.readTree.sumbtag >= sumbtag_min:
                continue

            #if not -log(self.readTree.y34) >= y34_min:
            #    continue
            
            #try:
            #    disid_int = int(dsid)
            #except ValueError:
                #catches the case of HHvvToOthers where int fails
            #    continue

            AllHistos[str(int(self.readTree.prodID))]["SumMj"].Fill(self.readTree.sumMj)
            AllHistos[str(int(self.readTree.prodID))]["weight"].Fill(self.readTree.weight)
            AllHistos[str(int(self.readTree.prodID))]["sumbtag"].Fill(self.readTree.sumbtag)
            try:
                HT = self.readTree.h1_pt_opt + self.readTree.h2_pt_opt
                #the "nocuts" ntuples do not contain those variables. Fill with dummy instead
            
            except:
                HT = 1
            
            AllHistos[str(int(self.readTree.prodID))]["HT"].Fill( HT )
            AllHistos[str(int(self.readTree.prodID))]["HTvsSumMj"].Fill( HT, self.readTree.sumMj )

            # # fill the abs(cos(theta)) for both of the higgses into one histogram
            # abscostheta_h1 = abs(cos(self.readTree.h1_theta_opt))
            # abscostheta_h2 = abs(cos(self.readTree.h2_theta_opt))
            # AllHistos[str(int(self.readTree.prodID))]["abs_cos_thetaH"].Fill(abscostheta_h1)
            # AllHistos[str(int(self.readTree.prodID))]["abs_cos_thetaH"].Fill(abscostheta_h2)

            if Has_BDT3:
                AllHistos[str(int(self.readTree.prodID))]["BDT"].Fill(self.readTree.BDT3)
                AllHistos[str(int(self.readTree.prodID))]["HTvsBDT"].Fill( HT, self.readTree.BDT3 )
                AllHistos[str(int(self.readTree.prodID))]["BDTvsSumMj"].Fill( self.readTree.BDT3 , self.readTree.sumMj )
                AllHistos[str(int(self.readTree.prodID))]["SumMjvsBDT"].Fill(  self.readTree.sumMj , self.readTree.BDT3 )

                
            
                



            #y34 just does not exist... this is because apparently no cut on it was applied!
            #AllHistos[str(int(self.readTree.prodID))]["y34"].Fill(self.readTree.y34)

            #AllHistos[str(int(self.readTree.prodID))]["SumMj"].Fill(self.readTree.sumMj, self.readTree.weight)
            #AllHistos[str(int(self.readTree.prodID))]["weight"].Fill(self.readTree.weight,self.readTree.weight)

            
        self.Histograms = AllHistos

        return

    #def passCuts(self,cutsConfig,event):
    #    return True

#    def readerLoop(self):
#        return self.readTree
#
#    def FillHHevent(self,tree,event):
#        self.HHEvent.BDT = tree.BDT3
#        self.HHEvent.SumMj = tree.sumMj
#        return 

        
