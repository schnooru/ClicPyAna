from clicpyana.AnalysisTools.HzqqEvent import HzqqEvent
from clicpyana.AnalysisTools.TTHEvent import TTHEvent
from clicpyana.AnalysisTools.WWEvent import WWEvent
from clicpyana.AnalysisTools.HmumuEvent import HmumuEvent
import ROOT
from clicpyana.ReadingTools import ReaderTools
from pyLCIO import IOIMPL
import json

class ReadSlcio:
    def __init__(self,configFile):
        self.HzqqEvent = HzqqEvent()
        self.WWEvent = WWEvent()
        self.TTHEvent = TTHEvent()
        self.HmumuEvent = HmumuEvent()
        if str(configFile).endswith("json"): 
            configDict = open(configFile).read()
            config = json.loads(configDict)
        else:
            config = configFile
        inputDir = config["InputTopDir"]
        print("Reading SLCIO files from {}".format(inputDir))
        self.LCreader = IOIMPL.LCFactory.getInstance().createLCReader()
        self.inputfiles = ReaderTools.walkInputDir(inputDir,"slcio")
        self.lastEvent = config["Nevents"]
        self.nentries=self.lastEvent
        if self.lastEvent == -1:
            self.nentries = config["NeventsTotalInAllSample"]
        readerloop = []

        return

    def readerLoop(self):
        for f in self.inputfiles:
            self.LCreader.open(str(f))
            for event in self.LCreader:
                yield event
            self.LCreader.close()
        
    def FillParticlesList(self, event,  name, PDGID=[0]):
        particlelist = []
        particleCollection = event.getCollection(name)
        for j in particleCollection:
            particle = ROOT.TLorentzVector()
            if not PDGID==[0]: 
                #use only those particles where the parameter "type" corresponds to the PDGID
                if not j.getType() in PDGID :
                    continue
            particle.SetPxPyPzE( j.getMomentum()[0], j.getMomentum()[1], j.getMomentum()[2], j.getEnergy() )
            particlelist.append(particle)
        return particlelist


    def FillHdecayproducts(self,event):
        mctruth = event.getCollection("MCParticlesSkimmed")
        for mcparticle in mctruth:
            if mcparticle.getPDG()==25 and len(mcparticle.getDaughters())>=2:
                return [mcparticle.getDaughters()[0].getPDG(),mcparticle.getDaughters()[1].getPDG()]
        return [0,0]
            
    def IsWjetsEvent(self,mctruthCollection):
        #event.getCollection("MCParticlesSkimmed"))
        for mcparticle in mctruthCollection:
            pass

    def FillDimuonMassTruth(self,event):
        if self.HzqqEvent.IsHmumu():
        #if self.HzqqEvent.cutmap["IsHmumuTruth"]:
            mctruth = event.getCollection("MCParticlesSkimmed")
            for mcparticle in mctruth:
                if mcparticle.getPDG()==25 and len(mcparticle.getDaughters())>=2:
                    mu0 = ROOT.TLorentzVector()
                    mu1 = ROOT.TLorentzVector()
                    m0 = mcparticle.getDaughters()[0]
                    m1 = mcparticle.getDaughters()[1]
                    mu0.SetPxPyPzE( m0.getMomentum()[0] , m0.getMomentum()[1],m0.getMomentum()[2], m0.getEnergy())
                    mu1.SetPxPyPzE( m1.getMomentum()[0] , m1.getMomentum()[1],m1.getMomentum()[2], m1.getEnergy())
                    return (mu0+mu1).M()
        return 0
    
    def GetY23Y34(self,event,jetcollection):
        n3jets = event.getCollection(jetcollection)
        #n=3, n+1=4, n-1=2
        y34 = n3jets.getParameters().getFloatVal("y_{n,n+1}")
        y23 = n3jets.getParameters().getFloatVal("y_{n-1,n}")
        return y23, y34
        

    def FillGeneralEvent(self,AnalysisEvent,event):
        AnalysisEvent.muonslist = self.FillParticlesList(event, "IsolatedLeptonCollection",PDGID=[13,-13])
        #AnalysisEvent.muonslist = self.FillParticlesList(event, "TightSelectedPandoraPFANewPFOs",PDGID=[13,-13])

        
        AnalysisEvent.electronslist = self.FillParticlesList(event, "IsolatedLeptonCollection",PDGID=[11,-11])
        #AnalysisEvent.photonslist = self.FillParticlesList(event, "PandoraPFANewPFOs",PDGID=[22])

        return AnalysisEvent

    def FillWWEvent(self,event):
        self.WWEvent =self.FillGeneralEvent(self.WWEvent,event)
        self.WWEvent.jetsList2 = self.FillParticlesList(event,"Jet_VLC_R07_N2")
        self.WWEvent.jetsList2_smeared = self.FillParticlesList(event,"Jet_VLC_R07_N2")
        self.WWEvent.dijetsmasslist = ReaderTools.DiParticleMasslist (self.WWEvent.jetsList2)
        self.WWEvent.jetsList3 = self.FillParticlesList(event,"Jet_VLC_R07_N3")
        self.WWEvent.jetsList4 = self.FillParticlesList(event,"Jet_VLC_R07_N4")
        self.WWEvent.y23, self.WWEvent.y34 = self.GetY23Y34(event,"Jet_VLC_R07_N3")
        #self.WWEvent.IsWjetsEvent = IsWjetsEvent(event.getCollection("MCParticlesSkimmed"))
        return self.WWEvent

    def FillTTHEvent(self,event):
        self.TTHEvent = self.FillGeneralEvent(self.TTHEvent,event)
        self.TTHEvent.jetsList6 = self.FillParticlesList(event,"Jet_VLC_R10_N6")
        self.TTHEvent.dijetsmasslist = ReaderTools.DiParticleMasslist (self.TTHEvent.jetsList6)
        self.TTHEvent.y23, self.TTHEvent.y34 = self.GetY23Y34(event,"Jet_VLC_R10_N3")


        return self.TTHEvent

    def FillHmumuEvent(self,event):
        self.HmumuEvent = HmumuEvent()
        self.HmumuEvent = self.FillGeneralEvent(self.HmumuEvent, event)
        self.HmumuEvent.dimuonmasslist = ReaderTools.DiParticleMasslist( self.HmumuEvent.muonslist )
        return self.HmumuEvent

        
    def FillHzqqEvent(self,event):
        self.HzqqEvent = self.FillGeneralEvent(self.HzqqEvent,event)
        self.HzqqEvent.jetsList3 = self.FillParticlesList(event,"Jet_VLC_R10_N3")
        self.HzqqEvent.jetsList4 = self.FillParticlesList(event,"Jet_VLC_R10_N4")
        self.HzqqEvent.jetsList5 = self.FillParticlesList(event,"Jet_VLC_R10_N5")
        self.HzqqEvent.y23, self.HzqqEvent.y34 = self.GetY23Y34(event,"Jet_VLC_R10_N3")
        self.HzqqEvent.dimuonmasslist = ReaderTools.DiParticleMasslist( self.HzqqEvent.muonslist )

        if not self.HzqqEvent.FindZcandidateJets([self.HzqqEvent.jetsList3,self.HzqqEvent.jetsList4,self.HzqqEvent.jetsList5  ]):
            print("did not find any useful Z candidate in the event")
        self.HzqqEvent.Recoil()
        self.HzqqEvent.Hdecay = self.FillHdecayproducts(event)

        self.HzqqEvent.mtruthmumu = self.FillDimuonMassTruth(event)
        
        return self.HzqqEvent
        
