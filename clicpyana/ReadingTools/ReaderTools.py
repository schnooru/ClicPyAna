import sys, os
import json

def configDictFromJson(configFile):
    configJson = open(configFile).read()
    try:
        configDict = json.loads(configJson)
    except ValueError as e:
        print("ValueError for json.loads in the file {}: check if json format is correct in that file".format(configFile))
        raise e
    return configDict


def DiParticleMasslist(particlelist):
    diparticlemasses = []
    #Needs to be a vector of TLorentzVectors

    for p1 in xrange(len(particlelist)):
        for p2 in xrange(len(particlelist)):
            if p1 <= p2:
                continue
            m = (particlelist[p1]+particlelist[p2]).M()
            #print("Mass is {}".format(m))
            #diparticlemasses.append(abs(m))
            diparticlemasses.append(m)
 
    return diparticlemasses

def walkInputDir(inputTopDir,suffix=".root",verbose=False):
    if verbose: print("Input directory {}\n=> Using all *{} files".format(inputTopDir, suffix))
    inputfiles = []
    for dirName, subdirList, fileList in os.walk(inputTopDir):
        for fname in fileList:
            if fname.endswith("{suffix}".format(suffix=suffix)):
                inputfiles.append(dirName +"/" + fname)
    if verbose: 
        print("Found the following input files: \n{}\n".format("\n".join(inputfiles)))
    return inputfiles
