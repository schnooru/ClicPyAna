import ROOT as r
from math import log10, log, cos

class ReadHHAnaChain:
    """reads the Anachain files from Rosa"""
    
    def  __init__(self, Histos, dsid, cutsDict, pathToFile):
        pathFileToRead = pathToFile
        # read the file, extract the number of events from it... this also needs to be normalized later TODO
        #fill histograms!
        
        ntupfi = r.TFile.Open(pathFileToRead)
        print (ntupfi.GetName())

        BDT_min = cutsDict["BDT_min"]
        sumbtag_min = cutsDict["sumbtag_min"]
        y34_min = cutsDict["y34_min"]

        varbranch = ntupfi.Get("var")
        self.nentries = varbranch.GetEntries()

        #find out if this file has a varbranch.BDT5, which would mean it is a BDT2018 file, and in that case do not use BDT3 but instead BDT5!!

        if not varbranch.GetBranch("BDT5"):
            #this is an ntuple with BDT2016
            IsBDT2016 = True
            print("This is a sample with BDT2016")
        else:
            IsBDT2016 = False
            print("This is a sample with BDT2018")


        for event in varbranch:

            #fill the values from the branches for this event
            SumMjvalue = varbranch.sumMj
            SumBtag    = varbranch.sumbtag
            h1theta    = varbranch.h1_theta_opt
            h2theta    = varbranch.h2_theta_opt

            
            if IsBDT2016:
                BDTvalue = varbranch.BDT3
            else:
                BDTvalue = varbranch.BDT5      

            # apply BDT cut          
            if BDT_min > -990 and not BDTvalue >= BDT_min:
                continue
            if not SumBtag >= sumbtag_min:
                continue
            #at the end, it was only cut at SumBtag and no cut on y34 was applied




            if not varbranch.y34 > 0.0:
                continue
            #print SumBtag, -log(varbranch.y34)
            #seems it's an OR, from Rosa's code??
            #if not  -log(varbranch.y34) >= y34_min:
            #if not (SumBtag >= sumbtag_min or -log(varbranch.y34) >= y34_min):
                #print "NOT keeping it"
                #print SumBtag, -log(varbranch.y34)
                #continue


            #-log10(y34) >= 3.6 for bbbb candidates
            #print varbranch.y34, -log10(varbranch.y34)
            Histos["SumMj"].Fill( SumMjvalue  )
            Histos["BDT"].Fill( BDTvalue  )
            Histos["sumbtag"].Fill( SumBtag  )
            Histos["y34"].Fill( varbranch.y34  )
            HT = varbranch.h1_pt_opt + varbranch.h2_pt_opt
            Histos["HT"].Fill( HT)
            Histos["HTvsBDT"].Fill( HT, BDTvalue  )
            Histos["HTvsSumMj"].Fill( HT, SumMjvalue  )
            Histos["BDTvsSumMj"].Fill ( BDTvalue, SumMjvalue)
            Histos["SumMjvsBDT"].Fill (  SumMjvalue,BDTvalue)
            Histos["abs_cos_thetaH"].Fill( abs(cos( h1theta)))
            Histos["abs_cos_thetaH"].Fill( abs(cos( h2theta)))

            
        self.Histograms = Histos     
        return

        
    def readerLoop(self):
        return range(0,self.nentries)
