from ROOT import TFile,gDirectory,gROOT, TH1F
import sys
from HHTools import dictAnaSample_FromCSV
from clicpyana.ReadingTools import ReaderTools

import os.path

class ReadDatasetHistos:
    """reads Histos containing events for one DSID and deals with them

    this class should be able to group them, extract the (weighted) event yields, and write combined signal+bg histograms
    """



    def __init__(self,AllHistos, AllHistosFileName, DataSetDict ):
        """ where AllHistosFileName should contain the histograms
        and DataSetDict the corresponding info on the DSIDs properties

        """

        print("Reading Dataset Histos from {}".format(AllHistosFileName))

        histofile = TFile.Open(AllHistosFileName)
        
        #need to as always, create TH1F objects??
        for dsid in DataSetDict.keys():

            AllHistos[dsid] = []


        #print AllHistos
        #loop over all histos in the file
        for h in histofile.GetListOfKeys():


            hname = h.GetName()
            
            #print hname

            
            dsid = self.DSID_from_hname(hname)

            hist = histofile.Get(hname)
            #print("dsid {}".format(dsid))
            hclone = hist.Clone()
            hclone.SetDirectory(0)
            AllHistos[dsid].append(hclone)


        self.Histograms = AllHistos
        #print("those are the histos {}".format(self.Histograms))
        return

        
    def DSID_from_hname(self,histname):
        DSID = histname.split("_")[ len(histname.split("_")  ) -1]
        return DSID


        