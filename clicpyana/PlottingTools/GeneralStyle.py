"""Define a general style for plots"""
from ROOT import gStyle, gPad, TText, kBlack, THStack, TLatex
def GetEntryFromDict(listOfKeyCandidates, Dictionary):
    for k in listOfKeyCandidates:
        if k in Dictionary:
            return Dictionary[k]
    return ""

def SaveCanvas( canv, histobject, name ):
    save_name = name

    
    canv.Modified()
    canv.Update()
    save_it = raw_input("Save this plot as {}? [y/N]".format(save_name)  )=="y"
    if save_it:
        canv.SaveAs("{}".format(name))
        canv.SaveAs("{}".format(name.replace(".pdf",".C")))


    if type(histobject) == THStack:
        for i in histobject.GetHists():

            i.Delete()
    del histobject

        
    canv.Close()
    return

def CreateSmallText(size=0.038):

    smallt = TLatex()
    smallt.SetTextSize(size)
    smallt.SetTextFont(42)
    smallt.SetTextColor(kBlack)
    
    return smallt

def SetGeneralStyle(canv,histo,histoConfig, WIP=False):
    canv.cd()

    Title = GetEntryFromDict(["Title","title"], histoConfig)
    xTitle = GetEntryFromDict(["xTitle","xtitle"], histoConfig)
    yTitle = GetEntryFromDict(["yTitle","ytitle"], histoConfig)
    
    #workaround: need to fix the wrong eta titles
    if "eta" in xTitle:
        xTitle = xTitle.replace("[GeV]","")
        # end of workaround
    gStyle.SetPalette(56) #105, 107, 57
    gStyle.SetNumberContours(50)
    gStyle.SetPadTickY(1)
    gStyle.SetPadTickX(1)

    try:
        histo.SetStats(0)
    except AttributeError:
        pass
    gPad.SetTicks(1,1)
    canv.SetBottomMargin(0.22)
    canv.SetLeftMargin(0.2)
    #histo.Draw("")
    xaxis = histo.GetXaxis()
    yaxis = histo.GetYaxis()
    histo.SetTitle(Title)
    xaxis.SetTitle(xTitle)
    xaxis.SetTitleSize(1.6*xaxis.GetTitleSize())
    yaxis.SetTitleSize(1.2*xaxis.GetTitleSize())
    yaxis.SetTitleOffset(1.6*yaxis.GetTitleOffset())
    xaxis.SetLabelSize(0.06)
    yaxis.SetLabelSize(0.06)
    yaxis.SetTitle(yTitle)
    xaxis.SetNdivisions(505)
        
    clictext = "CLICdp"
    if WIP:
        clictext += " Work in Progress"
    t1 = TText(0.5,0.5,clictext)
    t1.SetTextSize(0.05)
    t1.SetTextColor(kBlack)
    t1.SetTextFont(42)
    t1.DrawTextNDC(0.4,0.8, clictext)
    
    canv.Modified()
    canv.Update()
    
    return canv, histo, t1
