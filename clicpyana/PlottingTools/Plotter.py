from ROOT import TLegend, gStyle, TCanvas, TF1, TPad, TText, kBlack

def createCanvas(canv_name="canv",xwidth=500,ywidth=450):
    canv = TCanvas(canv_name,canv_name,xwidth,ywidth)

    return canv

def obtainPropertyHistogram(histogramname,propertyname,histoDict):
    #name = histogram.GetName()
    #print histoDict
    Property = histoDict[histogramname][propertyname]
    return Property
    
def obtainPropertySample(histogram,propertyname, configDicts):
    label = histogram.GetName().split("_")[-1]
    Property = 1
    for dic in configDicts:
        if dic["Label"] == label:
            Property = dic[propertyname]
    
    return Property
    
def fitInvariantMass( histogram ):

    
    fitfunc = TF1( 'aFunction', 'gaus', 125, 127 )
    fitfunc.SetParameter(0,1)
    fitfunc.SetParameter(1,126)
    fitfunc.SetParameter(2,1)
    fitresult = histogram.Fit(fitfunc, 'S')
    
    #print fitfunc.GetParameter(0)
    return fitfunc, fitresult, histogram
    
def plotComparisonNormalized(canvas,histlist,maxfact=1.5,WIP=True):
    return plotComparison(canvas,histlist,maxfact,DoNormalize=True,WIP=WIP)

def CLIC_WIP_Label():
    t1 = TText(0.5,0.5,"CLICdp Work in Progress"  )
    textsize=0.09
    t1.SetTextSize(textsize)
    t1.SetTextColor(kBlack)
    t1.SetTextFont(42)
    return t1

def plotComparison(canvas,histlist,maxfact=1.5,legendymin=0.7,textsize=0.09,DoNormalize=False,WIP=False):
    """Creates a plot with two histograms in it"""
    first = True


    t1 = TText(0.5,0.5,"CLICdp"  )

    t1.SetTextSize(textsize)
    t1.SetTextColor(kBlack)
    t1.SetTextFont(42)
    
    legend = TLegend(0.6,legendymin,0.9,0.9)
    legend.SetBorderSize(0)
    legend.SetFillStyle(0) #transparent
    gStyle.SetOptTitle(0)


    canvas.SetLeftMargin(0.15)
    canvas.SetBottomMargin(0.18)

    for h in histlist:

        #h.SetMarkerStyle(22)
        h.SetMarkerSize(4)
        h.SetLineWidth(4)

        if h.Integral()<0.00000001:
            continue
        if DoNormalize:
            h.Scale(1./h.Integral())
            h.SetMaximum(maxfact* h.GetMaximum())
        if not first:
            h.Draw("HIST E1 same")
        else:
            h.Draw("HIST E1")
            h.SetMaximum(maxfact* h.GetMaximum())
            h.SetMinimum(0)
        text = h.GetTitle()

        legend.AddEntry(h,text,"l")
        first = False


    
    legend.Draw()

    clictext = "CLICdp"
    if WIP:
        clictext += " Work in Progress"
    t1.DrawTextNDC(0.22,0.92, clictext  )
    canvas.Update()
    #canvas.GetTitle().Clear()
    return canvas, legend


def setFeaturesAxis(axis, titleoffset=0.7,titlesize=0.12,labelsize=0.2):
    axis.SetTitleSize(titlesize)
    #axis.SetTitleFont(43)
    axis.SetTitleOffset(titleoffset)
    #axis.SetLabelFont(43)
    axis.SetLabelSize(labelsize)
    return axis

                        
