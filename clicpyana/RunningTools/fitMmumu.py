import ROOT
from Plotter import plotComparison, createCanvas, obtainPropertySample, fitInvariantMass
import ReaderTools
import sys, os

def main(*args):

    nComparisons = len(args) -1
    canv = createCanvas()
    
    anadir = "/home/schnooru/Projects/Delphes/CLICdet-Delphes/MyAnalysisCode/"

    #runConfigFile = "/home/schnooru/Projects/Delphes/CLICdet-Delphes/MyAnalysisCode/Configs/runConfig_detVal.json"

    if len(args) > 1:
        runConfigFile = args[1]
    runConfig = ReaderTools.configDictFromJson(runConfigFile)
    
    
    configFiles = []
    configDicts = []

    if len(args)== 1:
        sys.exit("Please supply a run config, e.g. Configs/runConfig.json")
        #configFiles.append("/home/schnooru/Projects/Delphes/CLICdet-Delphes/MyAnalysisCode/slcioConfig.json")
        #print (" Using default config file(s) {}".format((", ").join(configFiles)))
        #configDicts.append(ReaderTools.configDictFromJson(configFiles[0] ))

    for readerConfigFile in runConfig["ReaderConfigs"]:
        readerConfigFilePath= runConfig["ConfigDir"] + readerConfigFile
        #runOverEventfile(runConfig,readerConfigFilePath)
        configDict=ReaderTools.configDictFromJson(readerConfigFilePath  )
        configDicts.append( configDict )


    plotdir = runConfig["PlotDir"]
    

    histodict = {}
    labellist = []
    #ROOT.gDirectory.cd()
    for cfg in configDicts:
        label = cfg["Label"]
        labellist.append(label)
        histoFileName =  cfg["HistoDir"] + "/histo"+label+"_"+runConfig["OverallTag"]+".root"
        print("Accessing Histogram file {}".format(histoFileName))
        histofile = ROOT.TFile.Open(histoFileName,"READ")
        for h in histofile.GetListOfKeys():
            h = h.ReadObj()
            #print h.GetName(), h.GetName().strip("_"+ label)
            #histoname_withoutlabel = h.GetName().strip("_"+ label)
            _histoname = h.GetName().split("_")
            del _histoname[-1]
            histoname_withoutlabel = ("_").join(_histoname)


            #histodict[h.GetName()] = {}
            if not histoname_withoutlabel in histodict:
                histodict[histoname_withoutlabel] = {}
            histodict[histoname_withoutlabel][label] = h
            #print (h, type(h), type(      histodict[h.GetName()][label]))
            histodict[histoname_withoutlabel][label].SetDirectory(0)
            #ROOT.gDirectory.cd()
        #print histodict
        histofile.Close()

        #print histodict    


    #print histodict

    #create the histogram to contain the plots
    plotsubdir = plotdir + ("vs").join(labellist) + "_"+ runConfig["OverallTag"]+"/"
    print("Will save the histograms in {}".format(plotsubdir))
    if not os.path.exists(plotsubdir):
        os.mkdir(plotsubdir)
    else:
        print("Overwriting plots in {}".format( plotsubdir ))


    histlist = []
    funclist = []
    print histodict["m_mumu_115_135"]
    for label in histodict["m_mumu_115_135"].keys():
        h= histodict["m_mumu_115_135"][label]

        h.SetLineColor(obtainPropertySample( h, "Color", configDicts ))
        h.GetXaxis().SetRangeUser(125,127)
        
        if h.Integral() >0:
            h.Scale(1./h.Integral())
        print ("Fit for {}:".format(label))
        fitfunc, fitresult, h = fitInvariantMass( h)
        
        fitfunc.SetLineColor(h.GetLineColor())
        fitfunc.Draw("l")
        
        print fitfunc.GetParameter(0)

        
        canv.Modified()
        canv.Update()

        histlist.append(h)
        funclist.append(fitfunc)
        #fitfile = open(plotsubdir + label + "_Mmumu_fit.txt",'w')
        #print("this is the text {}".format(fitresult.Write()))
        #fitfile.write(fitresult.Print())
        #fitfile.close()
        #h.Draw()
        #raw_input()
    canv,leg = plotComparison(canv,histlist) 
    for h in histlist:
        h.GetYaxis().SetRangeUser(0,0.5)
    for f in funclist:
        f.Draw("same")

        canv.Modified()
        canv.Update()


    canv.SaveAs(plotsubdir +"m_Hmumu_fit.pdf" )
    
    
            

    raw_input()

if __name__ == "__main__":
    main(*sys.argv)
  

