import ROOT
from math import log10
import sys, os
from ReadDelphes import ReadDelphes
from ReadSlcio import ReadSlcio
from HzqqEvent import HzqqEvent
from HistogramCreator import HistogramCreator
from Selection import Selection
from Plotter import plotComparisonNormalized, createCanvas
import ReaderTools
from ROOT import TH1F, TCanvas, TLorentzVector

def main(*args):
       
    eventReader = ReadDelphes("Configs/readerConfig_checkfile.json")
    
    #VLCjetR10N4
    nentries = eventReader.nentries
    nevent = 0
    h = TH1F("h","h",50,0,150)
    #h = TH1F("h","h",20,-5,5)
    c = TCanvas("canv","canv",800,600)
    for event in eventReader.readerLoop():
        nevent += 1
        if nevent % 200 == 0:
            print("Reading event n. {}".format(nevent))
        eventReader.treeReader.ReadEntry(event)

        #print len(eventReader.branchDict["truthparticles"])
        nlep = 0
        for p in eventReader.branchDict["truthparticles"]:

            #find the top
            if abs(p.PID)==6 and p.Status ==22:
                #find the W
                for tdaughter in [p.D1, p.D2]:
                    PDGID = eventReader.branchDict["truthparticles"][tdaughter].PID
                    if abs(PDGID)==24:
                        for decayprod in [eventReader.branchDict["truthparticles"][tdaughter].D1,eventReader.branchDict["truthparticles"][tdaughter].D2]:
                            if eventReader.branchDict["truthparticles"][decayprod].PID in [11,13,-11,-13]:
                                nlep +=1

        if nlep ==1:
#            for pp in eventReader.branchDict["truthparticles"]:
#                #print pp.PID
#                if abs(pp.PID)==5:
#                    h.Fill( pp.E )
            #loop over jets:
            btagged = 0
            for j in eventReader.branchDict["VLCjetR10N4"]:
                #if j.BTag >=4:
                #if j.BTag in [1,3,5,7]: #tight WP (50%eff)
                if j.BTag in [2,3,6,7]: #medium WP
                    btagged +=1
            if btagged ==2:
                for j in eventReader.branchDict["VLCjetR10N4"]:
                    if j.BTag in [1,3,5,7]: #tight WP (50%eff)
                        jet=TLorentzVector()
                        jet.SetPtEtaPhiM(j.PT, j.Eta, j.Phi, j.Mass)
                        #if they are b-tagged, fill the histo
                        h.Fill(jet.E() )
    h.Draw("E1")
    c.Modified()                
    c.Update()
    save_as = raw_input("Save as? ")
    c.SaveAs("../../CheckingRobertosFile/"+save_as)

if __name__ == "__main__":
    main(*sys.argv)
  