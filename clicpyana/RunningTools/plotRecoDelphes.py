import ROOT
from math import log10
import sys, os
from ReadDelphes import ReadDelphes
from ReadSlcio import ReadSlcio
from HzqqEvent import HzqqEvent
from HistogramCreator import HistogramCreator
from Selection import Selection
from Plotter import plotComparisonNormalized


def createCanvas():
    canv = ROOT.TCanvas("canv","canv",800,600)

    return canv


def main(*args):

    print(args[0])
    ROOT.gStyle.SetOptStat(0)
    anadir = "/home/schnooru/Projects/Delphes/CLICdet-Delphes/MyAnalysisCode/"

    
    canv = createCanvas()
    #delphesOutputsTopdir=args[1]
    delphesReader = ReadDelphes("../../CLICdet-Delphes/MyAnalysisCode/delphesConfig.json" )
    
    analysis = "HZqq" #should come from steering_dict
    delphesHists = HistogramCreator("../../CLICdet-Delphes/MyAnalysisCode/Histograms.json","Delphes").histsdict
    nEntriesDelphes = delphesReader.nentries
    nevent = 0
    for event in delphesReader.readerLoop(): #range(0, nEntriesDelphes):
        nevent+=1
        if nevent % 5000 == 0:
            print "Reading event number {} of {}".format(nevent,delphesReader.nentries)

        if nevent == delphesReader.lastEvent:
            break
        delphesHzqqEvent = delphesReader.FillHzqqEvent(event)
        if not Selection("../../CLICdet-Delphes//MyAnalysisCode/SelectionHzqq.json").passSelection(delphesHzqqEvent):
            continue 
        delphesHists["mZ"].Fill(delphesHzqqEvent.Zfourvec.M())
        delphesHists["mrecoil"].Fill(delphesHzqqEvent.Recoilvec.M())
        delphesHists["mrec_mZ"].Fill(delphesHzqqEvent.Recoilvec.M(),delphesHzqqEvent.Zfourvec.M()) 
        delphesHists["mrec_prec"].Fill(delphesHzqqEvent.Recoilvec.M(),delphesHzqqEvent.Recoilvec.P())
        delphesHists["precoil"].Fill(delphesHzqqEvent.Recoilvec.P())
        delphesHists["y23"].Fill(delphesHzqqEvent.y23)
        delphesHists["y34"].Fill(delphesHzqqEvent.y34)
        delphesHists["mH_true"].Fill(delphesHzqqEvent.mtruthmumu)

        #if len( delphesHzqqEvent.muonslist ) > 1:
        #    print("List of muons {}".format(delphesHzqqEvent.muonslist))
        #    print("List of dimuon masses {}".format(delphesHzqqEvent.dimuonmasslist))
        for mmumu in delphesHzqqEvent.dimuonmasslist:
            delphesHists["m_mumu"].Fill(mmumu)
            if mmumu>100:
                delphesHists["m_mumu_100"].Fill(mmumu)
                
        for dr in delphesHzqqEvent.dRjetbList:
            delphesHists["dRjetb"].Fill(dr)
        for drc in delphesHzqqEvent.dRjetcList:
            delphesHists["dRjetc"].Fill(drc)



    recoReader = ReadSlcio("../../CLICdet-Delphes//MyAnalysisCode/slcioConfig.json")
    recoHists = HistogramCreator("../../CLICdet-Delphes/MyAnalysisCode/Histograms.json","Fullsim").histsdict
    nevent = 0
    for event in recoReader.readerLoop():
        nevent+=1
        if nevent % 50 == 0:
            print "Reading event number {} of {}".format(nevent,recoReader.nentries)
        if nevent == recoReader.lastEvent:
            break
        recoHzqqEvent = recoReader.FillHzqqEvent(event)
        if not Selection("../../CLICdet-Delphes//MyAnalysisCode/SelectionHzqq.json").passSelection(recoHzqqEvent):
            continue 
        recoHists["mZ"].Fill(recoHzqqEvent.Zfourvec.M())
        recoHists["mrecoil"].Fill(recoHzqqEvent.Recoilvec.M())
        recoHists["mrec_mZ"].Fill(recoHzqqEvent.Recoilvec.M(),recoHzqqEvent.Zfourvec.M()) 
        recoHists["mrec_prec"].Fill(recoHzqqEvent.Recoilvec.M(),recoHzqqEvent.Recoilvec.P())
        recoHists["precoil"].Fill(recoHzqqEvent.Recoilvec.P())
        recoHists["y23"].Fill(recoHzqqEvent.y23)
        recoHists["y34"].Fill(recoHzqqEvent.y34)
        recoHists["mH_true"].Fill(recoHzqqEvent.mtruthmumu)

        for mmumu in recoHzqqEvent.dimuonmasslist:
            recoHists["m_mumu"].Fill(mmumu)
            if mmumu>100:
                recoHists["m_mumu_100"].Fill(mmumu)

    canv,leg = plotComparisonNormalized(canv,[delphesHists["mrecoil"],recoHists["mrecoil"]])
    canv.Update()
    canv.SaveAs(anadir+"//delphes_plots/mrecoil.pdf")

    canv,leg = plotComparisonNormalized(canv,[delphesHists["mZ"],recoHists["mZ"]])
    canv.Update()
    canv.SaveAs(anadir+"//delphes_plots/mZ.pdf")

  
  
    
    
    canv.SetRightMargin(0.14)
    delphesHists["mrec_mZ"].Draw("colz")
    canv.Update()
    canv.SaveAs(anadir+"//delphes_plots/mrec_mZ_delphes.pdf")

    recoHists["mrec_mZ"].Draw("colz")
    canv.Update()
    canv.SaveAs(anadir+"//delphes_plots/mrec_mZ_reco.pdf")

    recoHists["mrec_prec"].Draw("colz")
    canv.Update()
    canv.SaveAs(anadir+"//delphes_plots/mrec_prec_reco.pdf")
    delphesHists["mrec_prec"].Draw("colz")
    canv.Update()
    canv.SaveAs(anadir+"//delphes_plots/mrec_prec_delphes.pdf")
                                    
    #canv = plotComparisonNormalized(canv,[delphesHists["precoil"],recoHists["precoil"]])
    canv.Update()
    canv.SetRightMargin(0.1)

    canv,leg = plotComparisonNormalized(canv,[delphesHists["dRjetb"]])
    canv.Update()
    canv.SaveAs(anadir+"//delphes_plots/dRjetb.pdf")
    canv,leg = plotComparisonNormalized(canv,[delphesHists["dRjetc"]])
    canv.Update()
    canv.SaveAs(anadir+"//delphes_plots/dRjetc.pdf")

    canv,leg = plotComparisonNormalized(canv,[delphesHists["mH_true"], recoHists["mH_true"]])
    canv.Modified()
    canv.Update()
    canv.SaveAs(anadir+"//delphes_plots/mH_true.pdf")


    canv,leg = plotComparisonNormalized(canv,[delphesHists["m_mumu"],recoHists["m_mumu"]])
    canv.Modified()
    canv.Update()
    canv.SaveAs(anadir+"//delphes_plots/m_mumu.pdf")

    canv,leg = plotComparisonNormalized(canv,[delphesHists["m_mumu_100"],recoHists["m_mumu_100"]])
    canv.Modified()
    canv.Update()
    canv.SaveAs(anadir+"//delphes_plots/m_mumu_100.pdf")



    canv,leg = plotComparisonNormalized(canv,[delphesHists["y23"],recoHists["y23"]])
    canv.Update()
    canv.SaveAs(anadir+"//delphes_plots/y23.pdf")

    canv,leg = plotComparisonNormalized(canv,[delphesHists["y34"],recoHists["y34"]])
    canv.Update()
    canv.SaveAs(anadir+"//delphes_plots/y34.pdf")
    

    
    raw_input()

  
if __name__ == "__main__":
    main(*sys.argv)
  

