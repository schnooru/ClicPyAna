import ROOT
from PlottingTools.Plotter import plotComparison, plotComparisonNormalized, createCanvas, obtainPropertySample, obtainPropertyHistogram, setFeaturesAxis
from ReadingTools import ReaderTools
import sys, os

def main(*args):

    nComparisons = len(args) -1
    canv = createCanvas()
    
    if len(args) > 1:
        runConfigFile = args[1]
    runConfig = ReaderTools.configDictFromJson(runConfigFile)
    
    
    configFiles = []
    configDicts = []

    if len(args)== 1:
        sys.exit("Please supply a run config, e.g. Configs/runConfig.json")

    for readerConfigFile in runConfig["ReaderConfigs"]:
        readerConfigFilePath= runConfig["ConfigDir"] + readerConfigFile
        configDict=ReaderTools.configDictFromJson(readerConfigFilePath  )
        configDicts.append( configDict )

    anadir = runConfig["AnalysisDir"]
    histoconfigDict = ReaderTools.configDictFromJson( runConfig["ConfigDir"] + runConfig["HistoConfig"] )
    plotdir = runConfig["PlotDir"]

    histodict = {}
    labellist = []
    for cfg in configDicts:
        label = cfg["Label"]
        labellist.append(label)
        histoFileName =  cfg["HistoDir"] + "/histo"+label+"_"+runConfig["OverallTag"]+".root"
        print("Accessing Histogram file {}".format(histoFileName))
        if not os.path.exists(histoFileName):
            sys.exit("File {} does not exist".format(histoFileName))
        histofile = ROOT.TFile.Open(histoFileName,"READ")
        #histofile.cd()
        for h in histofile.GetListOfKeys():
            h = h.ReadObj()
            #print h.GetName(), h.GetName().strip("_"+ label)
            #histoname_withoutlabel = h.GetName().strip("_"+ label)
            _histoname = h.GetName().split("_")
            del _histoname[-1]
            histoname_withoutlabel = ("_").join(_histoname)

            if not histoname_withoutlabel in histodict:
                histodict[histoname_withoutlabel] = {}
            histodict[histoname_withoutlabel][label] = h

            histodict[histoname_withoutlabel][label].SetDirectory(0)
            #ROOT.gDirectory.cd()

        histofile.Close()

    #create the histogram to contain the plots
    plotsubdir = plotdir + ("vs").join(labellist) + "_"+ runConfig["OverallTag"]+"/"
    print("Will save the histograms in {}".format(plotsubdir))
    if not os.path.exists(plotsubdir):
        os.mkdir(plotsubdir)
    else:
        print("Overwriting plots in {}".format( plotsubdir ))


    for histtitle in histodict:
        histlist = []
        for l in histodict[histtitle].keys():
            _h = histodict[histtitle][l]
            #print(histtitle, l, configDicts)
            _h.SetLineColor(obtainPropertySample( _h, "Color", configDicts ))
            try:
                _h.Rebin(obtainPropertyHistogram(histtitle, "rebin", histoconfigDict))
            except:
                pass
            _h.GetXaxis().SetRangeUser(obtainPropertyHistogram(histtitle, "xbinlow", histoconfigDict),obtainPropertyHistogram(histtitle, "xbinup", histoconfigDict)  )




            _h.SetMaximum(1.2* _h.GetMaximum())
            histlist.append(_h)


        if type(histlist[0]) == ROOT.TH1F and not runConfig["comparemode"] == "ratio":
            canv,leg = plotComparisonNormalized(canv,histlist)
            canv.Update()
            canv.SaveAs(plotsubdir +histtitle+".pdf"  )
        if type(histlist[0]) == ROOT.TH1F and runConfig["comparemode"] == "ratio":
            #create upper pad
            newcanv = createCanvas("ratio_canvas")
            histpad = ROOT.TPad("histpad","histpad",0,0.4,1,1.)
            histpad.SetBottomMargin(0)
            histpad.SetLeftMargin(0.25)
            
            # dummy again
            histpad.Draw()
            histpad.cd()
            
            #cd in the upper pad, plot Comparison there
            newcanv,leg = plotComparisonNormalized(newcanv,histlist)

            newcanv.cd()

            histpad.cd()
            #adjust the axis to avoid overlap with the ratio axis
            #get the axis and set its labels to 0 size
            oldlabelsize = histlist[0].GetYaxis().GetLabelSize()
            histlist[0].GetYaxis().SetLabelSize(0.)
            ndivisions = 212
#
            histlist[0].GetYaxis().SetNdivisions(ndivisions)
#            
            mini = histlist[0].GetMinimum()
            maxi = histlist[0].GetMaximum()
            xmin = histlist[0].GetXaxis().GetXmin()

            myaxis = ROOT.TGaxis( xmin, mini+0.05*maxi, xmin, maxi-0.05*maxi, mini+0.05*maxi, maxi-0.05*maxi, ndivisions, "-"  )
            myaxis = setFeaturesAxis(myaxis, labelsize=oldlabelsize)

            myaxis.Draw()
            canv.Modified()
            canv.Update()

            newcanv.cd()
          

            #create the other pad
            ratiopad = ROOT.TPad("ratiopad","ratiopad", 0, 0.05,1, 0.4)
            ratiopad.SetTopMargin(0)
            ratiopad.SetBottomMargin(0.4)
            ratiopad.SetLeftMargin(0.25)
            ratiopad.SetGridy()
            ratiopad.Draw()
            ratiopad.cd()
            #there, plot the ratio
            if len(histlist)==2:
                   rationom = histlist[1].Clone()
                   rationom.Divide(histlist[0])
                   rationom.SetMinimum(0.6)
                   rationom.SetMaximum(1.5-0.01)
                   rationom.SetMarkerColor(ROOT.kBlack)
                   rationom.SetMarkerSize(1.5)
                   rationom.SetMarkerStyle(5)
                   rationom.SetFillColor(ROOT.kOrange-2)
                   rationom.GetXaxis().SetNdivisions(305)
                   rationomxaxis = setFeaturesAxis ( rationom.GetXaxis() ,titlesize= 0.14, titleoffset=0.8,labelsize=0.12)
                   rationom.GetYaxis().SetTitle("")
                   rationom.GetYaxis().SetNdivisions(505)
                   rationom.GetYaxis().SetTitle("#frac{Delphes}{FullSim}")
                   rationomyaxis = setFeaturesAxis ( rationom.GetYaxis(), titlesize=0.16, titleoffset=0.4,labelsize=0.12)
                   rationom.Draw("E2")
            newcanv.Update()
            newcanv.SaveAs(plotsubdir +histtitle+"_ratio.pdf"  )
            newcanv.SaveAs(plotsubdir +histtitle+"_ratio.C"  )
        canv.cd()
        if type(histlist[0]) == ROOT.TH2F:
            for l in histodict[histtitle].keys():
                print l
                histodict[histtitle][l].Draw("colz")
                canv.SetBottomMargin(0.2)
                canv.SetLeftMargin(0.25)

                canv.Modified()
                canv.Update()
                canv.SaveAs(plotsubdir +histtitle+"_"+l+".pdf"  )


    
            

    raw_input()

if __name__ == "__main__":
    main(*sys.argv)
  

