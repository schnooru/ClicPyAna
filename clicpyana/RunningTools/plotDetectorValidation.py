import ROOT
from math import log10
import sys, os
from ReadDelphes import ReadDelphes
from ReadSlcio import ReadSlcio
from HzqqEvent import HzqqEvent
from HistogramCreator import HistogramCreator
from Selection import Selection
from Plotter import plotComparisonNormalized, createCanvas



def main(*args):

    #print(args[0])
    ROOT.gStyle.SetOptStat(0)

    anadir = "/home/schnooru/Projects/Delphes/CLICdet-Delphes/MyAnalysisCode/"
    
    canv = createCanvas()
    #dst2558OutputsTopdir=args[1]
    dst2558Reader = ReadSlcio(anadir+"slcio_2558.json" )
    
    dst2558Hists = HistogramCreator(anadir+"Histograms.json","dst2558").histsdict
    nEntriesDst2558 = dst2558Reader.nentries
    nevent = 0
    for event in dst2558Reader.readerLoop(): #range(0, nEntriesDst2558):
        nevent+=1
        if nevent % 500 == 0:
            print "Reading event number {} of {}".format(nevent,dst2558Reader.nentries)

        if nevent == dst2558Reader.lastEvent:
            break
        dst2558HzqqEvent = dst2558Reader.FillHzqqEvent(event)
        if not Selection(anadir+"SelectionHzqq.json").passSelection(dst2558HzqqEvent):
            continue 
        dst2558Hists["mZ"].Fill(dst2558HzqqEvent.Zfourvec.M())
        dst2558Hists["mrecoil"].Fill(dst2558HzqqEvent.Recoilvec.M())
        dst2558Hists["mrec_mZ"].Fill(dst2558HzqqEvent.Recoilvec.M(),dst2558HzqqEvent.Zfourvec.M()) 
        dst2558Hists["mrec_prec"].Fill(dst2558HzqqEvent.Recoilvec.M(),dst2558HzqqEvent.Recoilvec.P())
        dst2558Hists["precoil"].Fill(dst2558HzqqEvent.Recoilvec.P())
        dst2558Hists["pZ"].Fill(dst2558HzqqEvent.Zfourvec.P())
        dst2558Hists["y23"].Fill(dst2558HzqqEvent.y23)
        dst2558Hists["y34"].Fill(dst2558HzqqEvent.y34)
        dst2558Hists["mH_true"].Fill(dst2558HzqqEvent.mtruthmumu)
        for mmumu in dst2558HzqqEvent.dimuonmasslist:
            dst2558Hists["m_mumu"].Fill(mmumu)
            if mmumu>100:
                dst2558Hists["m_mumu_100"].Fill(mmumu)
            if mmumu > 115 and mmumu < 135:
                dst2558Hists["m_mumu_115_135"].Fill(mmumu)

    dst9400Reader = ReadSlcio(anadir+"/slcio_9400.json" )
    
    dst9400Hists = HistogramCreator(anadir+"Histograms.json","dst9400").histsdict
    nEntriesDst9400 = dst9400Reader.nentries
    nevent = 0
    for event in dst9400Reader.readerLoop(): #range(0, nEntriesDst9400):
        nevent+=1
        if nevent % 500 == 0:
            print "Reading event number {} of {}".format(nevent,dst9400Reader.nentries)

        if nevent == dst9400Reader.lastEvent:
            break
        dst9400HzqqEvent = dst9400Reader.FillHzqqEvent(event)
        if not Selection(anadir+"SelectionHzqq.json").passSelection(dst9400HzqqEvent):
            continue 
        dst9400Hists["mZ"].Fill(dst9400HzqqEvent.Zfourvec.M())
        dst9400Hists["mrecoil"].Fill(dst9400HzqqEvent.Recoilvec.M())
        dst9400Hists["mrec_mZ"].Fill(dst9400HzqqEvent.Recoilvec.M(),dst9400HzqqEvent.Zfourvec.M()) 
        dst9400Hists["mrec_prec"].Fill(dst9400HzqqEvent.Recoilvec.M(),dst9400HzqqEvent.Recoilvec.P())
        dst9400Hists["precoil"].Fill(dst9400HzqqEvent.Recoilvec.P())
        dst9400Hists["pZ"].Fill(dst9400HzqqEvent.Zfourvec.P())
        dst9400Hists["y23"].Fill(dst9400HzqqEvent.y23)
        dst9400Hists["y34"].Fill(dst9400HzqqEvent.y34)
        dst9400Hists["mH_true"].Fill(dst9400HzqqEvent.mtruthmumu)
        for mmumu in dst9400HzqqEvent.dimuonmasslist:
            dst9400Hists["m_mumu"].Fill(mmumu)
            if mmumu > 100:
                dst9400Hists["m_mumu_100"].Fill(mmumu)
            if mmumu > 115 and mmumu < 135:
                dst9400Hists["m_mumu_115_135"].Fill(mmumu)

    canv,leg = plotComparisonNormalized(canv,[dst2558Hists["mrecoil"],dst9400Hists["mrecoil"]])
    canv.Update()
    canv.SaveAs(anadir+"valid_plots/mrecoil.pdf")


    
    canv,leg = plotComparisonNormalized(canv,[dst2558Hists["mZ"],dst9400Hists["mZ"]])
    canv.Update()
    canv.SaveAs(anadir+"valid_plots/mZ.pdf")

    
    canv,leg = plotComparisonNormalized(canv,[dst2558Hists["y23"],dst9400Hists["y23"]])
    canv.Update()
    canv.SaveAs(anadir+"valid_plots/y23.pdf")


    
    canv,leg = plotComparisonNormalized(canv,[dst2558Hists["y34"],dst9400Hists["y34"]])
    canv.Update()
    canv.SaveAs(anadir+"valid_plots/y34.pdf")

    canv,leg = plotComparisonNormalized(canv,[dst2558Hists["m_mumu"],dst9400Hists["m_mumu"]])
    canv.Update()
    canv.SaveAs(anadir+"valid_plots/m_mumu.pdf")

    canv,leg = plotComparisonNormalized(canv,[dst2558Hists["m_mumu_100"],dst9400Hists["m_mumu_100"]])
    canv.Update()
    canv.SaveAs(anadir+"valid_plots/m_mumu_100.pdf")
   
    canv,leg = plotComparisonNormalized(canv,[dst2558Hists["m_mumu_115_135"],dst9400Hists["m_mumu_115_135"]])
    canv.Update()
    canv.SaveAs(anadir+"valid_plots/m_mumu_115_135.pdf")
   
    
    canv.SetRightMargin(0.14)

    dst2558Hists["mrec_mZ"].Draw("colz")
    canv.Update()
    canv.SaveAs(anadir+"valid_plots/mrec_mZ_dst2558.pdf")

    dst9400Hists["mrec_mZ"].Draw("colz")
    canv.Update()
    canv.SaveAs(anadir+"valid_plots/mrec_mZ_dst9400.pdf")

    dst9400Hists["mrec_prec"].Draw("colz")
    canv.Update()
    canv.SaveAs(anadir+"valid_plots/mrec_prec_dst9400.pdf")

    
    dst2558Hists["mrec_prec"].Draw("colz")
    canv.Update()
    canv.SaveAs(anadir+"valid_plots/mrec_prec_dst2558.pdf")
                                    
    #canv = plotComparisonNormalized(canv,[delphesHists["precoil"],recoHists["precoil"]])
    canv.Update()


    
    raw_input()

  
if __name__ == "__main__":
    main(*sys.argv)
  

