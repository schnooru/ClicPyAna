from ROOT import RooRealVar,RooDataHist, RooArgSet,RooArgList, RooHistPdf, RooLinkedList,RooChi2Var, RooMinuit, RooPlot, RooAbsReal, TH1F, TMath, gROOT, TRandom3
from clicpyana.PlottingTools import Plotter
from array import array
from math import sqrt, exp, log
import csv

class TemplateFit:
    """ Get Histos for BG, SM and non-SM signals and template fit them """
    def __init__(self,HistBG, HistSigSM, DictHistSignals, TemplateObjects, randgen, isThrowToys):

        self.HistBG             = HistBG
        self.HistSigSM          = HistSigSM
        self.DictHistSignals    = DictHistSignals

        self.TemplateObjects    = TemplateObjects
        self.randgen            = randgen
        self.isThrowToys        = isThrowToys
        return

    def ToyVaryHist(self, hist_orig, randgen):#_GetRandom
        """Varies the Histogram content by repeating GetRandom N times, where N is the Gaussian variation
        of the total number according to the statistical error
        """
        hist_toy = hist_orig.Clone()
        nevents = 0
        for b in xrange(hist_toy.GetNbinsX()+2):
            nevents+=hist_toy.GetBinContent(b)
            hist_toy.SetBinContent(b,0)
        #fill this hsitogram with random numbers drawn from the original one, with the same total number (additionally, the total number must be varied acc. to Poisson/Gauss)
        ###############################
        ##Poissonian:
        #nevents = int(nevents)
        #nevents = randgen.Poisson(nevents)
        ###############################
        ##Gaussian:
        nevents = randgen.Gaus(nevents,sqrt(nevents))
        nevents = int(nevents)
        ###############################

        print("Throwing {} random numbers".format(nevents))
        for toyn in xrange(nevents):
            rndnumb = hist_orig.GetRandom()
            hist_toy.Fill(rndnumb)


        return hist_toy

    def ToyVaryHist_binContent(self, hist_orig , randgen): #_binContent
        """ returns a histogram with bin-by-bin poissonian variation w.r.t. the original histogram"""
        hist_toy = hist_orig.Clone()
        for hbin in xrange(hist_orig.GetNbinsX()+1):
            nev_bin =  hist_orig.GetBinContent(hbin)
            nev_bin_poisson = randgen.Poisson(nev_bin)
            #nev_bin_poisson = randgen.Gaus(nev_bin,sqrt(nev_bin))
            hist_toy.SetBinContent(hbin, nev_bin_poisson)
            #print ("old: {} new: {}".format(nev_bin, nev_bin_poisson))
        return hist_toy

    def GetChisquaresOfHistos(self, histname,addZhhtoChi2=False,zhh_dict={},addStg2HHvvtoChi2=False,HHvvStg2_dict={} ):
        """ Get all the chisquares for the histos given in self.DictHistSignals
        the user can choose to give all or just the 1D ones
        put them in a dictionary according to the dsids
        """
        chi2dict = {}

        errordict = {}
        #creating the SM histogram including HH->all
        SMHist = self.HistBG.Clone()
        SigSMHist = self.HistSigSM.Clone()
        SMHist.Add(SigSMHist)

        if self.isThrowToys:
            SMHist = self.ToyVaryHist(SMHist , self.randgen)
        #gROOT.SetBatch(False)
        #testcanvas = Plotter.createCanvas("c1")
        #SMHist.Draw()
        #raw_input()
        if addZhhtoChi2:
            randgen     = TRandom3()
        for i in self.DictHistSignals:

            h = self.DictHistSignals[i]

            if not h.GetName().split("_")[0]  == histname:
                continue
            dsid = h.GetName().split("_")[1]
            isSM= False
            if dsid == "7181":
                isSM = True
            #print("For DSID {}:".format(dsid))
            
            
            chi2, abserror = self.ChisquareHistogram( h, SMHist ,isSM) #normaly, the observedhisto should be the SMHist
            #print("chi2 for ghhh {} before adding zhh: {} ".format( self.TemplateObjects.gHHH_from_DSID (dsid)    , chi2))
            dontUse3rdStage = False
            if dontUse3rdStage:
                print("Not Using Any Info From 3rd Stage")
                chi2, abserror = 0, 0
            if addZhhtoChi2:
                chi2_zhh, abserror_zhh = self.Chi2andErrorFromZhh(dsid,zhh_dict,randgen, self.isThrowToys)
                chi2     += chi2_zhh
                abserror += abserror_zhh
                print("Considering ZHH 1.4TeV in Chi2")
            #if chi2 ==0:
            #    print("WARNING ZHH AT 1.4TeV NOT CONSIDERED!")
            if addStg2HHvvtoChi2:
                chi2_HHvvStg2, abserror_HHvvStg2 = self.Chi2andErrorFromHHvvStg2(dsid,HHvvStg2_dict,randgen, self.isThrowToys)
                chi2     += chi2_HHvvStg2
                abserror += abserror_HHvvStg2
                print("Considering HHvv 1.4TeV in Chi2")
           #if chi2 ==0:
            #    print("WARNING HHvv AT 1.4TeV ALSO NOT CONSIDERED!")

            chi2dict[dsid] = chi2
            errordict[dsid] = abserror
            #print("chi2 for ghhh {} after adding zhh: {} ".format( self.TemplateObjects.gHHH_from_DSID (dsid)    , chi2))
       
        return chi2dict, errordict

    def Chi2andErrorFromHHvvStg2(self,dsid, HHvv14_dict,randgen, ThrowToys):
        chi2          = 0
        abserror      = 0
        gHHH          = float( self.TemplateObjects.gHHH_from_DSID (dsid)  )
        lumi          = HHvv14_dict["lumi"]
        polfactor     = HHvv14_dict["polarization_factor"]
        n_signal_1500 = 16.3
        n_bg_1500     = 37

        So    = n_signal_1500/1500.* lumi *polfactor
        sigmaOverSigmaSM = self.ScaleHHvv14Tev(gHHH)
        if sigmaOverSigmaSM ==0:
            return 0,0
        Se    = n_signal_1500/1500.* lumi* polfactor*sigmaOverSigmaSM
        B     = n_bg_1500/1500.*lumi*polfactor
        No = So+B
        Ne = Se+B
        if ThrowToys:
            No            = self.VaryGaussianNobserved(No,randgen)

        chi2 = self.chi2InBin(Ne,No)
        if not No ==0:
            abserror      = 1./sqrt(No)*chi2
        print("this makes a chi2 summand of {} for gHHH={}".format(chi2,gHHH))
        return chi2, abserror

    def ScaleHHvv14Tev(self,gHHH):
        sigma_gHHHSM = 0.149
        if gHHH == 0.5:
            sigma = 0.24
        elif gHHH == 0.8:
            sigma = 0.179
        elif gHHH == 1.0:
            sigma = sigma_gHHHSM
        elif gHHH == 1.2:
            sigma = 0.128
        elif gHHH == 1.5:
            sigma = 0.12
        elif gHHH == 2.0:
            sigma = 0.14
        else:
            sigma = 0
            print("WARNING DID NOT FIND A HHvv(1.4TeV) CROSS SECTION FOR gHHH={} -> COULD LEAD TO FLUCTUATIONS IN CHI2".format(gHHH))

        print("----For HHvv at 1.4TeV, setting sigma={} for gHHH={}".format(sigma,gHHH))
        return sigma/sigma_gHHHSM

    def Chi2andErrorFromZhh(self,dsid, zhh_dict,randgen, ThrowToys):
        chi2          = 0
        abserror      = 0
        gHHH          = float( self.TemplateObjects.gHHH_from_DSID (dsid)  )
        lumi          = zhh_dict["lumi"]
        polfactor     = zhh_dict["polarization"]
        zhh_file      = zhh_dict["zhh_file"]
        

        xsec_zhh_ghhh = self.ZhhXsecFromCsvFile(zhh_file, gHHH)
        xsec_zhh_SM   = self.ZhhXsecFromCsvFile(zhh_file, 1.0)
        #No should be the SM case! this is also what is varied in the toys
        BR_hbb        = 0.58
        BR_Z_visible  = 0.8
        analysis_eff  = 0.5

        
        So            = xsec_zhh_SM*lumi*polfactor*BR_hbb*BR_hbb*analysis_eff*BR_Z_visible
        Se            = xsec_zhh_ghhh*lumi*polfactor*BR_hbb*BR_hbb*analysis_eff*BR_Z_visible
        #background is estimated to be three times the SM signal
        #No = So + 3*So 
        #Ne = Se + 3*So
        # can assume that it is background free
        No = So
        Ne = Se
        if ThrowToys:
            No            = self.VaryGaussianNobserved(No,randgen)

        chi2 = self.chi2InBin(Ne,No)
        if not No ==0:
            abserror      = 1./sqrt(No)*chi2
        
        return chi2, abserror


    def VaryGaussianNobserved(self,Nobs, randgen):

        Nobs_varied = randgen.Gaus(Nobs,sqrt(Nobs))
        #print(Nobs, Nobs_varied)
        return Nobs_varied



    def ZhhXsecFromCsvFile(self,zhh_file, gHHH):
        """Returns the cross section for the available lambdas."""
        xsec=0
        csvfile = open(zhh_file,'rb')
        reader = csv.reader(csvfile)
        #print(gHHH)
        for line in reader:
            if line[0].startswith("#"): continue
            gHHH_in_file = float(line[0].strip())
            if gHHH == gHHH_in_file:
                xsec = float(line[1].strip())
        if xsec == 0:
            print("WARNING DID NOT FIND A ZHH CROSS SECTION FOR gHHH={} -> COULD LEAD TO FLUCTUATIONS IN CHI2".format(gHHH))
        return xsec

    def ChisquareHistogram( self, SigHisto, ObservedHisto,isSM):


        # create the SM and couplings total histos
        CouplHist_t = self.HistBG.Clone()
        #### now add the histogram of the coupling histo
        hcoup = SigHisto.Clone()
        CouplHist_t.Add(hcoup)

        # loop over their bins and calculate the chi2
        xfirst = CouplHist_t.GetXaxis().GetFirst()
        xlast  = CouplHist_t.GetXaxis().GetLast() 
        #xlow   = CouplHist_t.GetXaxis().GetBinLowEdge( xfirst  )
        #xup    = CouplHist_t.GetXaxis().GetBinUpEdge(  xlast   )
        chi2         = 0
        errorsquared = 0

        for bin in xrange(xfirst,xlast+1):
            Ne = CouplHist_t.GetBinContent(bin)
            No = ObservedHisto.GetBinContent(bin)
            if No == 0:
                print("Warning: empty bin")
                continue

            chi2InBin = self.chi2InBin(Ne,No)
            chi2 += chi2InBin
            BG=  self.HistBG.GetBinContent(bin)
            #print("Ne={}, No={}, chi2={}, B={}, S=Nobs-B={}".format(Ne, No, chi2InBin, BG,  No -BG ))
            #print( bin,self.chi2InBin(Ne,No), self.NLLikelihoodInBin(Ne,No) )
            #chi2 += self.NLLikelihoodInBin(Ne,No)
            ###errorsquared += chi2InBin*1./sqrt(No)
            errorsquared += self.ErrorSquaredInBin(No,Ne,chi2InBin)
        abserror = sqrt(errorsquared)
        ###withouterrorbars: abserror = 0
        return chi2, abserror

    def ErrorSquaredInBin(self,Nio,Nie,chi2i):
        if Nie ==0:
            return 0
        #this is what I used for the YR and the CLICdp-Note:
        #return sqrt(Nie)*( 1-2* Nio**2/Nie**2 + Nio**4/Nie**4  ) +4*chi2i*sqrt(Nio)/Nie
        #there was definitely a mistake, it should be    
        #return Nie*( 1-2* Nio**2/Nie**2 + Nio**4/Nie**4  ) +4*chi2i*Nio/Nie
        # and also, Nie does not have an error:
        return 4*chi2i*Nio/Nie

    def NLLikelihoodInBin(self,Ne,No):
        likelihood = TMath.Gaus( No, Ne, sqrt(Ne) )

        return - log(likelihood)

    def chi2InBin(self,Ne,No):
        if Ne>0:
            chi2 = ((Ne-No)**2)/Ne
        else:
            print("WARNIING Ne = 0 in this bin")
            chi2 =0
        return chi2
        
    def ChisquareHistogramRooFit( self, SigHisto ):
        """Calculates the chisquare for one of the signal histos"""

        

        xlow = SigHisto.GetXaxis().GetBinLowEdge( SigHisto.GetXaxis().GetFirst()  )
        xup = SigHisto.GetXaxis().GetBinUpEdge( SigHisto.GetXaxis().GetLast()  )


        myvar = RooRealVar("x","x",xlow, xup)
        vars   =  RooArgList(myvar) #gives the dimensionalities of the histogram
       
#first, create a RooDataHist containing BG+couplingsSignal
        CouplHist_t = self.HistBG.Clone()

        #### now add the histogram of the coupling histo
        hcoup = SigHisto.Clone()

        CouplHist_t.Add(hcoup)

        CouplHist   = RooDataHist("sig","sig",vars, CouplHist_t)

        # make a RooHistPdf out of this RooDataHist ("pdf for hypothesis")
        argset = RooArgSet(myvar)
        couplPdf = RooHistPdf("sigpdf","sigpdf",argset,CouplHist)



 #then,create a RooDataHist containing the SM histogram HistBG+HistSigSM ("measured")
        SMHist_t = self.HistBG.Clone()
        SMHist_t.Add(self.HistSigSM.Clone())

        SMHist_t.Scale(1./CouplHist_t.Integral())
        SMHist =  RooDataHist("sm","sm", vars, SMHist_t)

        
        #frame = RooPlot(x.frame(Title("Chi^2 fit")) )
        testcanvas = Plotter.createCanvas()
                
        testcanvas.cd()
        xframe = RooPlot( )
        xframe = myvar.frame()
        couplPdf.plotOn(xframe)#in blue

        SMHist.plotOn(xframe)  #black points
        xframe.Draw()

        # the pdf is scaled to 1 over the entire range or so,
        # in any case it is lower than the SM histogram.
        # For the Chi2 that does not cause any problems because the offset is removed.
        
        dsid = SigHisto.GetName().split("_")[1]
        gHHH      = float( self.TemplateObjects.gHHH_from_DSID (dsid)  )
        gHHWW     = float( self.TemplateObjects.gHHWW_from_DSID(dsid)  )

        print("Performing the fit for gHHH={} and gHHWW={}".format(gHHH, gHHWW))


        # this could also be changed to a likelihood (NLL)
        chi2 = RooChi2Var( "chi2","chi2", couplPdf,SMHist  )

        m2 = RooMinuit(chi2)
        m2.migrad()
        print chi2.getVal()
        #raw_input()
        
        return chi2.getVal()
