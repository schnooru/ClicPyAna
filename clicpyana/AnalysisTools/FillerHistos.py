
def FillerHistos(readerConfig, AnalysisEvent,Histos):
    if readerConfig["Analysis"] == "HZqq":
        return HZFillHistos(AnalysisEvent,Histos)
    if readerConfig["Analysis"] == "WW":
        return WWFillHistos(AnalysisEvent,Histos)
    if readerConfig["Analysis"] == "ttH":
        return TTHFillHistos(AnalysisEvent,Histos)
    elif readerConfig["Analysis"] == "Hmumu":
        return HMMFillHistos(AnalysisEvent,Histos)


def TTHFillHistos(TTHEvent,Histos):
    #for now, no new histos added
     
    Histos =  WWFillHistos(TTHEvent,Histos)
    for j in TTHEvent.jetsList6:
        Histos["Eta_j"].Fill(j.Eta())
        Histos["Phi_j"].Fill(j.Phi())
        Histos["PT_j"].Fill(j.Pt())
        Histos["E_j"].Fill(j.E())
        Histos["PT_Eta_j"].Fill( j.Pt(), j.Eta() )

    Histos["N_b"].Fill(TTHEvent.nbtag)
    Histos["N_mu"].Fill(len(TTHEvent.muonslist))
    Histos["N_e"].Fill(len(TTHEvent.electronslist))
    Histos["N_lep"].Fill(len(TTHEvent.electronslist)+len(TTHEvent.muonslist))
    return Histos

def WWFillHistos(WWEvent,Histos):
    Histos["y23"].Fill(WWEvent.y23)
    Histos["y34"].Fill(WWEvent.y34)
    Histos["N_mu"].Fill(len(WWEvent.muonslist))
    Histos["N_e"].Fill(len(WWEvent.electronslist))
    Histos["N_lep"].Fill(len(WWEvent.electronslist)+len(WWEvent.muonslist))
    
    for m in WWEvent.muonslist:
        Histos["Eta_mu"].Fill(m.Eta())
        Histos["Phi_mu"].Fill(m.Phi())
        Histos["PT_mu"].Fill(m.Pt())
        Histos["E_mu"].Fill(m.E())
        Histos["PT_Eta_mu"].Fill( m.Pt(), m.Eta() )
    for e in WWEvent.electronslist:
        #if e.Pt() < 10:
        #    continue
        Histos["Eta_e"].Fill(e.Eta())
        Histos["Phi_e"].Fill(e.Phi())
        Histos["PT_e"].Fill(e.Pt())
        Histos["E_e"].Fill(e.E())
        Histos["PT_Eta_e"].Fill( e.Pt(), e.Eta() )
    for ph in WWEvent.photonslist:
        Histos["Eta_gamma"].Fill(ph.Eta())
        Histos["Phi_gamma"].Fill(ph.Phi())
        Histos["PT_gamma"].Fill(ph.Pt())
        Histos["E_gamma"].Fill(ph.E())
    for j in WWEvent.jetsList2:
        Histos["Eta_j"].Fill(j.Eta())
        Histos["Phi_j"].Fill(j.Phi())
        Histos["PT_j"].Fill(j.Pt())
        Histos["E_j"].Fill(j.E())
        Histos["PT_Eta_j"].Fill( j.Pt(), j.Eta() )

    for mqqW in WWEvent.dijetsmasslist:
        Histos["m_jj"].Fill(mqqW)
    for m in WWEvent.truth_muonslist:
        Histos["PT_truth_mu"].Fill(m.Pt())
        Histos["Eta_truth_mu"].Fill(m.Eta())
        Histos["PT_truth_lep"].Fill(m.Pt())
        Histos["Eta_truth_lep"].Fill(m.Eta())
    for e in WWEvent.truth_electronslist:
        Histos["PT_truth_e"].Fill(e.Pt())
        Histos["Eta_truth_e"].Fill(e.Eta())
        Histos["PT_truth_lep"].Fill(e.Pt())
        Histos["Eta_truth_lep"].Fill(e.Eta())

    return Histos

def HMMFillHistos(HMMEvent,Histos):
    
    for mmumu in HMMEvent.dimuonmasslist:
        Histos["m_mumu"].Fill(mmumu)
        if mmumu>100:
            Histos["m_mumu_100"].Fill(mmumu)
        if mmumu > 115 and mmumu < 135:
            Histos["m_mumu_115_135"].Fill(mmumu)
        if mmumu > 80 and mmumu < 110:
            Histos["m_mumu_80_110"].Fill(mmumu)
    for m in HMMEvent.muonslist:
        #if m.Pt() < 10:
        #    continue
        Histos["Eta_mu"].Fill(m.Eta())
        Histos["Phi_mu"].Fill(m.Phi())
        Histos["PT_mu"].Fill(m.Pt())
        Histos["E_mu"].Fill(m.E())
        Histos["PT_Eta_mu"].Fill( m.Pt(), m.Eta() )
        #need double loop!!
        for mm in HMMEvent.muonslist:
            if m == mm:
                continue
            Histos["mmumu_pTmu"].Fill( (m+mm).M(), m.Pt() )
            Histos["mmumu_Etamu"].Fill( (m+mm).M(), m.Eta() )


    return Histos

def HZFillHistos(HZQQEvent,Histos):
    Histos["m_Z"].Fill(HZQQEvent.Zfourvec.M())
    Histos["mrecoil"].Fill(HZQQEvent.Recoilvec.M())
    Histos["mrec_mZ"].Fill(HZQQEvent.Recoilvec.M(),HZQQEvent.Zfourvec.M()) 
    Histos["mrec_prec"].Fill(HZQQEvent.Recoilvec.M(),HZQQEvent.Recoilvec.P())
    Histos["precoil"].Fill(HZQQEvent.Recoilvec.P())
    Histos["p_Z"].Fill(HZQQEvent.Zfourvec.P())
    Histos["y23"].Fill(HZQQEvent.y23)
    Histos["y34"].Fill(HZQQEvent.y34)
    #if HZQQEvent.IsHbb():
    #    Histos["mH_bb"].Fill(HZQQEvent.)
    Histos["mH_true"].Fill(HZQQEvent.mtruthmumu)
    #print("H mass {}".format(HZQQEvent.Hbbfourvec.M() ))
    Histos["mHbb"].Fill( HZQQEvent.Hbbfourvec.M() )
    
    for mmumu in HZQQEvent.dimuonmasslist:
        Histos["m_mumu"].Fill(mmumu)
        if mmumu>100:
            Histos["m_mumu_100"].Fill(mmumu)
        if mmumu > 115 and mmumu < 135:
            Histos["m_mumu_115_135"].Fill(mmumu)
        if mmumu > 80 and mmumu < 110:
            Histos["m_mumu_80_110"].Fill(mmumu)

    for j in HZQQEvent.jetsList4:
        Histos["Eta_j"].Fill(j.Eta())
        Histos["Phi_j"].Fill(j.Phi())
        Histos["PT_j"].Fill(j.Pt())
        Histos["E_j"].Fill(j.E())
        Histos["PT_Eta_j"].Fill( j.Pt(), j.Eta() )
        Histos["mrec_pTj"].Fill( HZQQEvent.Recoilvec.M(),j.Pt()   )
        Histos["mrec_etaj"].Fill( HZQQEvent.Recoilvec.M(),j.Eta()   )
        Histos["mHbb_pTj"].Fill( HZQQEvent.Hbbfourvec.M(),j.Pt()   )
        Histos["mHbb_etaj"].Fill( HZQQEvent.Hbbfourvec.M(),j.Eta()   )
        
    for m in HZQQEvent.muonslist:
        #if m.Pt() < 10:
        #    continue
        Histos["Eta_mu"].Fill(m.Eta())
        Histos["Phi_mu"].Fill(m.Phi())
        Histos["PT_mu"].Fill(m.Pt())
        Histos["E_mu"].Fill(m.E())
        Histos["PT_Eta_mu"].Fill( m.Pt(), m.Eta() )
        #need double loop!!
        for mm in HZQQEvent.muonslist:
            if m == mm:
                continue
            Histos["mmumu_pTmu"].Fill( (m+mm).M(), m.Pt() )
            Histos["mmumu_Etamu"].Fill( (m+mm).M(), m.Eta() )

    for e in HZQQEvent.electronslist:
        #if e.Pt() < 10:
        #    continue
        Histos["Eta_e"].Fill(e.Eta())
        Histos["Phi_e"].Fill(e.Phi())
        Histos["PT_e"].Fill(e.Pt())
        Histos["E_e"].Fill(e.E())
        Histos["PT_Eta_e"].Fill( e.Pt(), e.Eta() )
    for ph in HZQQEvent.photonslist:
        Histos["Eta_gamma"].Fill(ph.Eta())
        Histos["Phi_gamma"].Fill(ph.Phi())
        Histos["PT_gamma"].Fill(ph.Pt())
        Histos["E_gamma"].Fill(ph.E())

    for m in HZQQEvent.truth_muonslist:
        Histos["PT_truth_mu"].Fill(m.Pt())
        Histos["Eta_truth_mu"].Fill(m.Eta())
    for e in HZQQEvent.truth_electronslist:
        Histos["PT_truth_e"].Fill(e.Pt())
        Histos["Eta_truth_e"].Fill(e.Eta())

    return Histos