
from HzqqEvent import HzqqEvent
import json

class Selection:
    """Applies a selection which can be configured by requiring certain cuts/flags in HZqqEvent to be passed.
    Needs to be configured with a configuration json"""
    def __init__(self, selectionConfig):
        #configDict = open(configJson).read()
        self.config = selectionConfig
        
        return

    def passSelection(self, Event):
        #for k in Event.cutmap.keys():
        Event.fillCutmap()
        for k in self.config["cuts"]:
            if not Event.cutmap[str(k)]:
                return False

        return True

