from ROOT import TLorentzVector
from math import log10
from Event import Event

class HzqqEvent(Event):
    """Event properties of HZ->Hqq events"""

    y23 = 999
    y34 = 999
    mZ = 91.2
    CME = 350 #GeV
    
    def __init__(self):
        Event.__init__(self)
        self.jetsList2=[]
        self.jetsList3 =[] #should contain a list of 4vectors which are the jets... but are 4vectors enough?
        self.jetsList4 =[]
        self.jetsList5 =[]


        self.dimuonmasslist = []


        self.truth_electronslist=[]
        self.truth_muonslist=[]

        
        self.dRjetbList = [] #used for delphes check of dR(jet,bquark)
        self.dRjetcList = [] #used for delphes check of dR(jet,c quark)
        self.Hdecay = [0,0] #pdg IDs of the decay products
        self.Zdecay = [0,0]

        self.mtruthmumu = 0
        
        self.comment ="a HZ->Hqq event"
        self.Zfourvec = TLorentzVector()
        self.ZJet1 = TLorentzVector()
        self.ZJet2 = TLorentzVector()
        #always 2 ZJets

        #could be more or less than 2 Hjets:
        self.HbbJets = []

        self.Hbbfourvec = TLorentzVector()


        return

    def fillCutmap(self):
        self.cutmap["Y23Y34"] = self.passY23Y34()
        self.cutmap["mZ"] = self.passMZ()
        self.cutmap["mRecoil"] = self.passRecoilMass()
        self.cutmap["IsHmumuTruth"] = self.IsHmumu()
        self.cutmap["IsHbbTruth"] = self.IsHbb()
        self.cutmap["ZjetsCentral"] = self.PassZjetsCentral()
        self.cutmap["ZjetsForward"] = self.PassZjetsForward()
        self.cutmap["AllJetsCentral"] = self.PassAllJetsCentral()
        self.cutmap["AllJets10GeV"] = self.PassJetsAbove10GeV()
        self.cutmap["CentralMuons"] = self.PassBarrelMuons()
        return

   
    def PassBarrelMuons(self):
        for m in self.muonslist:
            if abs(m.Eta()) > 0.76:
                return False
        return True

#    def PassPtElectron10GeV(self):
#        for e in self.electronslist:
#            if e.Pt() < 10:
#                return False
#        return True
#
    def passY23Y34(self):
        passcut = log10(self.y23) > - 2.0 or log10(self.y34) > - 3.0
        return passcut

    def PassZjetsCentral(self):
        passcut = self.ZJet1.Eta() < 2 and self.ZJet2.Eta < 2
        return passcut
    
    def PassZjetsForward(self):
        passcut = self.ZJet1.Eta() > 2 and self.ZJet2.Eta > 2
        return passcut

    def PassJetsAbove10GeV(self):
        passcut = True
        for jet in self.jetsList4:
            if jet.Pt() < 10:
                passcut = False
        return passcut
        
    def PassAllJetsCentral(self):
        passcut = True
        for jet in self.jetsList4:
            if abs(jet.Eta()) > 1.7:
                passcut = False
        return passcut
        

    def passMZ(self):
        #self.FindZcandidateJets([self.jetsList3, self.jetsList4, self.jetsList5])
        #selection mus be run after FillHzqqEvent, otherwise this, and all other cuts, do not work
        passcut = ( self.Zfourvec.M() > 70 and self.Zfourvec.M() <  110)
        return passcut

    def passRecoilMass(self):
        self.Recoil()
        passcut = self.Recoilvec.M() > 80 and self.Recoilvec.M() < 200
        return passcut

    def Recoil(self):
        self.Recoilvec = TLorentzVector()

        self.Recoilvec.SetE( self.CME - self.Zfourvec.E() )
        self.Recoilvec.SetPx( - self.Zfourvec.Px())
        self.Recoilvec.SetPy( - self.Zfourvec.Py())
        self.Recoilvec.SetPz( - self.Zfourvec.Pz())
        return True
        
    def FindZcandidateJets(self,jetslists):
        #find the best dijet Z candidate from N=3,4,5 jets and fill the Z 4vector as well as the recoil 4vector

        mbestZ = 0
        for jetlist in jetslists:
            useForHbb = False
            if len(jetlist)==4:
                useForHbb = True
            
            ZJ1, ZJ2, HbbJets = self.ClosestZcandidate(jetlist)

            Zcand = ZJ1+ZJ2
            if self.closerToZ(Zcand.M(), mbestZ):
                mbestZ = Zcand.M()
                self.Zfourvec = Zcand
                self.ZJet1 = ZJ1
                self.ZJet2 = ZJ2

            if useForHbb:
                #print("Using 4 jets and getting Hbb")
                #print len(HbbJets)
                self.HbbJets = HbbJets

        Hbbfourvec = TLorentzVector()
        #print ("how many Hbb jets {}".format(len(self.HbbJets)))
        i=0
        for j in self.HbbJets:
            i+=1

            Hbbfourvec += j
            #print("{} adding the next jet, mass {}".format(i,Hbbfourvec.M()))
        

        
        self.Hbbfourvec = Hbbfourvec
        #print("final mass {}".format( self.Hbbfourvec.M()))
        return mbestZ >0.01


    def ClosestZcandidate(self,listOfJets):
        ZJ1 = TLorentzVector()
        ZJ2 = TLorentzVector()
        HbbJets = []
        mCand = 0
        for i in listOfJets:
            for j in listOfJets:
                if i== j:
                    continue
                mjj = (i+j).M()
                if self.closerToZ(mjj, mCand  ):
                    ZJ1 = i
                    ZJ2 = j
                    mCand = mjj
        HbbJ1 = 0
        HbbJ2 = 0
        if len(listOfJets) >=2: #only then Hbb is really useful
            if ZJ1 in listOfJets and ZJ2 in listOfJets:
                listOfJets.remove(ZJ1)
                listOfJets.remove(ZJ2)
            HbbJets = listOfJets

        return ZJ1, ZJ2, HbbJets

            
                    
    def closerToZ(self,mass_new, mass_previous):
        return abs(mass_new - self.mZ) < abs(mass_previous - self.mZ)

    def IsHmumu(self):
        #if self.Hdecay == [13,-13] or self.Hdecay == [-13,13]:
            #print "Hmumu truth event with muons pts: {}".format(",".join(str(i.Pt()) for i in self.muonslist) )
            #print( "Dimuonmass list {}".format( ",".join(str(m) for m in self.dimuonmasslist)))
        
        #a cut which says whether or not it is a H-> mumu event. add to cutmap!
        return self.Hdecay == [13,-13] or self.Hdecay == [-13,13]
        
    def IsHbb(self):
        #a cut which says whether or not it is a H-> bb event. add to cutmap!
        return self.Hdecay == [5,-5] or self.Hdecay == [-5,5]
