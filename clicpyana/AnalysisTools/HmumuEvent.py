from ROOT import TLorentzVector
from Event import Event

class HmumuEvent(Event):
    """Event properties of VBF H->mumu events"""

    def __init__(self):
        Event.__init__(self)
        self.dimuonmasslist = []
        return


    def fillCutmap(self):
        self.cutmap["CentralMuons076"] = self.PassCentralMuons(0,0.76)
        self.cutmap["CentralMuons132"] = self.PassCentralMuons(0,1.32)
        self.cutmap["CentralMuons132to174"] = self.PassCentralMuons(1.32,1.74)
        self.cutmap["CentralMuons174to266"] = self.PassCentralMuons(1.74,2.66)
        return

    def PassCentralMuons(self,etamin,etamax):
        oneNotInThisBin = False
        for m in self.muonslist:
            if abs(m.Eta())  < etamin or abs(m.Eta()) > etamax:
                oneNotInThisBin = True
                
        return not oneNotInThisBin