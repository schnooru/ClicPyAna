from ROOT import TLorentzVector
from math import log10
from Event import Event

class TTHEvent(Event):
    """Event properties of tt_H->lnubbqq_bb events"""

    y23 = 999
    y34 = 999
    mW = 80
    CME = 3000 #GeV
    
    def __init__(self):
        Event.__init__(self)
        self.jetsList2=[]
        self.dijetsmasslist =[]
        self.jetsList3 =[] #needed for y23, y34
        self.jetsList4 =[]
        self.jetsList6 =[]

        self.nbtag = 0

        return

    def fillCutmap(self):
        self.cutmap["AllJets10GeV"] = self.PassJetsAbove10GeV()
        return

    def PassJetsAbove10GeV(self):
        passcut = True
        for jet in self.jetsList4:
            if jet.Pt() < 10:
                passcut = False
        return passcut
        
