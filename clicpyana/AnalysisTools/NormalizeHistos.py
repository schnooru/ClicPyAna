from ROOT import TH1F
import sys
from math import sqrt

class NormalizeHistos:
    def __init__(self, samplesDict_Sig, samplesDict_BG, histos_nocuts_Sig_theo, histos_presel_Sig, histos_BDT_Sig, histos_nocuts_BG, histos_presel_BG, histos_BDT_BG, processGroupDict_BG, processGroupDict_Sig ,luminosity, polarizationFactor, verbose=False ):

        
        self.samplesDict_Sig      =      samplesDict_Sig
        self.samplesDict_BG       =       samplesDict_BG
        
        self.SigGroupDict         = processGroupDict_Sig 

        self.histos_nocuts_Sig_theo    =    histos_nocuts_Sig_theo #this actually does not contain anything
        self.histos_presel_Sig    = histos_presel_Sig
        self.histos_BDT_Sig       = histos_BDT_Sig
        self.histos_nocuts_BG     = histos_nocuts_BG
        self.histos_presel_BG     = histos_presel_BG
        self.histos_BDT_BG        = histos_BDT_BG
        self.polarizationFactor   = polarizationFactor
        
        self.eff_anavar = {'6025': 0.302 , '6776': 0.7460 ,  '6751': 0.9372 ,  '6754': 0.2763 ,  '6540': 0.4110 ,  '2660': 0.9480 ,  '3048' : 0.4613 ,  '4586' : 0.8317 ,  '6747' : 0.4210 ,  '6785': 0.8320 ,  '6782': 0.8212 ,  '6779': 0.8173 ,  '6736' : 0.3876 ,  '6742' : 0.3870 ,  '6733' : 0.2352 ,  '6739' : 0.2361 ,  '6574' : 0.9024 ,  '6580' : 0.9024 ,  '6571' : 0.5426 ,  '6577' : 0.5431 ,  '6942' : 0.4684 ,  '6945' : 0.7676 ,  '6948' : 0.4676 ,  '6951' : 0.7677 ,  '6730': 0.8315 ,  '6727': 0.5449 ,  '6724': 0.5443 ,  '6721' : 0.3710 ,  '6724' : 0.,  'HHvvToOthers' : 0.  }
        self.lumi = luminosity
        self.verbose = verbose
        return


    def SetTheorySigNocuts(self, histo_dict_TheorySigNocuts):
        self.histos_nocuts_Sig_theo = histo_dict_TheorySigNocuts
        return

    def NormalizeTheorySigNocuts(self, signal_samples_dict):
        NormDict =  self.NormDict_BeforeSelection( self.histos_nocuts_Sig_theo, signal_samples_dict) #should basically contain sigma*lumi
        self.histos_nocuts_Sig_theo = self.NormalizeAllHistos_WithNormdict( self.histos_nocuts_Sig_theo, NormDict)
        return

    def SetLumi(self, luminosity):
        self.lumi = luminosity
        return
        
    def GrepDSID(self,orig_printout,DSID):
        printout =""

        for line in orig_printout.split("\n"):
            if line.startswith(str(DSID)):
                printout += line
        return printout
        
    def PrintHistosEntries(self, HistosDict):
        #print HistosDict
        printout = ""

        for dsid in HistosDict.keys():
            N_entries = 0
            N_entries = self.GetEntries_ForDSID(dsid,HistosDict)
            printout += "{}  {}\n".format(dsid,N_entries)
        return printout

        
    def PrintHistosIntegrals(self, HistosDict):
        #print HistosDict
        printout = ""

        for dsid in HistosDict.keys():
            N_yields = 0
            N_yields = self.GetIntegral_ForDSID(dsid,HistosDict)
            printout += "{}  {:.3f}\n".format(dsid,N_yields)
        return printout
    

    def NormalizeSig(self):
        print("Normalizing the signal samples using lumi = {} /fb".format(self.lumi))

        #first, group the histograms to take into account extended statistics
        self.histos_BDT_Sig = self.GroupHistograms( self.histos_BDT_Sig, self.SigGroupDict )


        self.histos_BDT_Sig = self.NormalizeAllHistos_WithNormdict( self.histos_BDT_Sig, self.NormDict_BDTHistosFromNsample( self.histos_BDT_Sig, self.samplesDict_Sig ,Treesplit = False ) )
        return
        
    def NormalizeBG(self, NormalizationMode):
        print("Normalizing the background samples using method: {} with lumi = {} /fb".format(NormalizationMode, self.lumi))
        if NormalizationMode == "fromNsample":

            self.histos_BDT_BG = self.NormalizeAllHistos_WithNormdict(self.histos_BDT_BG, self.NormDict_BDTHistosFromNsample( self.histos_BDT_BG, self.samplesDict_BG  ) )
        elif NormalizationMode == "AnaVarMethod":
            self.histos_BDT_BG = self.NormalizeAllHistos_WithNormdict(self.histos_BDT_BG, self.NormDict_BDTHistosAnaVarMethod( self.histos_nocuts_BG, self.samplesDict_BG  ) )

        return 


    def NormalizeAllHistos_WithNormdict(self,HistoDict, NormDict):
        """
        takes a histo dictionary and normalizes each one according to the normalization dictionary (dependent on DSID)
        """
        #print("HistoDict {}".format(HistoDict))
        #print("NormDict {}".format(NormDict))
        #for dsid in HistoDict.keys():
        #    print dsid
        #print NormDict
        NormalizedHistoDict = {}
        for dsid in HistoDict.keys():
            #HistoDict[dsid] is a list of histogram objects, which should be scaled
            NormalizedHistoDict[dsid] = []
            try:
                NormDict[dsid]
            except:
                print("No info about how to normalize DSID {}. Do nothing.".format(dsid))
                continue
                
            for h in HistoDict[dsid]:
                integral = h.Integral()
                if integral == 0:
                    continue

                normfactor = NormDict[dsid]

                h.Scale(normfactor/integral )
                NormalizedHistoDict[dsid].append(h.Clone())
        
        return NormalizedHistoDict

    def NormDict_BeforeSelection(self, histosToUse, sampleDict):
        NormDict = {}
        if not histosToUse:
            sys.exit("ERROR For the normalization, need the histograms for selection")
        for dsid in histosToUse:
            sigma = sampleDict[dsid]["xsec"]
            lumi_scale = self.Lumi_scale(dsid)
            polarizationFactor = self.PolarizationFactor(dsid)
            NormDict[dsid] = self.lumi* lumi_scale * polarizationFactor * sigma

        return NormDict
    def NormDict_BDTHistosAnaVarMethod( self, histosToUse, sampleDict ):
        """this uses only the histos from nocuts level, testTree, BG"""
        NormDict = {}
        eff_anavar = self.eff_anavar

        factor_treesplit = 2
        if not histosToUse:
            sys.exit("ERROR For the AnaVarMethod, the histograms file for BG in nocuts stage needs to be provided. In config: Proc_BG_nocuts_histoFile. Skipping the normalization step completely and exiting!!!!!")

        for dsid in histosToUse:
            
            sigma = sampleDict[dsid]["xsec"]

            lumi_scale = self.Lumi_scale(dsid)
            polarizationFactor = self.PolarizationFactor(dsid)
            #Nraw normally should be the nocuts file
            Nraw = 0
            Nraw = self.GetEntries_ForDSID(dsid,histosToUse  )
            NrawBDT = self.GetEntries_ForDSID(dsid, self.histos_BDT_BG)
            #print("{} {}".format(dsid, Nraw))
            if Nraw > 0:
                NormDict[dsid] = self.lumi * lumi_scale* polarizationFactor *sigma  * eff_anavar[dsid] * NrawBDT/ float(Nraw)

            else:
                NormDict[dsid] =0
            #print("dsid: {}, eff_anavar: {}, NrawBDT: {}, Nraw: {}".format(dsid,eff_anavar[dsid],NrawBDT,Nraw))
        return NormDict

    def GetNtotalRaw(self, dsid, sampleDict):
        #if the dsid shows up in the signal group config, use the sum!!
        #this is not done for the backgrounds
        ntotalraw_dsid = 0
        if dsid in self.SigGroupDict.keys():
            for d in self.SigGroupDict[dsid]:
                ntotalraw_dsid +=sampleDict[dsid]["nrawtotal"]
        else:
            ntotalraw_dsid = sampleDict[dsid]["nrawtotal"]
        return ntotalraw_dsid


    def NormDict_BDTHistosFromNsample( self, histosToNormalize, sampleDict, Treesplit=True ):
        """ Normalization of DSID sample as
        N = lumi * sigma * N_raw(BDT,TestTree)/(k_Treesplit * N_raw(sample) )

        normally, histosToNormalize should be self.histos_BDT_BG
        """

        # needs:
        ### the histograms from BG in BDT region (presel would also work)
        ### also, needs cross section, and Ntotal(sample) from the csv, as well as
        ### the correction factor k_Treesplit, which might have to be added to the csv once I found it!!!
        ### for now using factor 2
        
        NormDict = {}
        #if type(self.histos_BDT_Sig) == None:
        #    print("Proc_Sig_BDT_histoFile not found")
        #    return NormDict
        if type(histosToNormalize) == None:
            print("Proc_BG/Sig_BDT_histoFile not found")
            return NormDict   

        factor_Treesplit =1
        if Treesplit:
            factor_Treesplit = 2
        #print sampleDict.keys()

        for dsid in histosToNormalize:
            sigma = sampleDict[dsid]["xsec"]
            Ntotalraw = self.GetNtotalRaw(dsid, sampleDict)

            lumi_scale = self.Lumi_scale(dsid)
            polarizationFactor = self.PolarizationFactor(dsid)
            if self.verbose:
                print("For DSID {}, using polarization factor {}".format(dsid, polarizationFactor))
            Nraw = 0
            Nraw = self.GetEntries_ForDSID(dsid,histosToNormalize  )

            NwBDT = self.lumi * lumi_scale*polarizationFactor* sigma *factor_Treesplit* float(Nraw)/float(Ntotalraw)
            NormDict[dsid] = NwBDT
            #        self.histos_BDT_Sig =#SHOULD BE UPDATED
            if self.verbose:
                print("dsid: {}, treesplit: {}, Nraw: {}, Ntotalraw: {},".format(dsid,factor_Treesplit, Nraw, Ntotalraw))
        return NormDict   

    def Lumi_scale(self, DSID):
        lumi_scale =1
        egamma_dsids = ["6736", "6742","6733","6739","6574","6580","6571","6577","6945","6951","6942","6948"]
        gammagamma_dsids = ["6730","6727","6724","6721"]
        if DSID in egamma_dsids:
            lumi_scale=0.79
        elif DSID in gammagamma_dsids:
            lumi_scale=0.69
        return lumi_scale

    def PolarizationFactor(self,DSID,polarizationSignalOnly=False):
        polarizationFactor = 1.

        if not polarizationSignalOnly:
            return self.polarizationFactor
        #otherwise, normalize only the signal:
        signal_dsids = ["7181","6025"]
        for dsid in self.SigGroupDict.keys():
            signal_dsids.append(dsid)
        if DSID in signal_dsids:
            polarizationFactor = self.polarizationFactor
        print("using a polarization factor of {}".format(polarizationFactor))
        return polarizationFactor
        

    def GetIntegral_ForDSID(self,DSID,HistosDict):
        return self.GetEventyields_ForDSID(DSID,HistosDict,"Integral")


    def GetEntries_ForDSID(self,DSID,HistosDict):
       return self.GetEventyields_ForDSID(DSID,HistosDict,"Entries")

    def GetEventyields_ForDSID(self, DSID, HistosDict, mode="Integral"):
        """DSID can be an int or str"""
        N_yield = 0

        for h in HistosDict[str(DSID)]:
            if h.GetName() == "SumMj_{}".format(DSID):
                if mode=="Integral":
                    N_yield = h.Integral()
                elif mode == "Entries":
                    N_yield = h.GetEntries()
        return N_yield


    def GroupResultsTable(self, NormalizedHistosDict,  procGroupConfig):


        printout = ""
        for proc in procGroupConfig.keys():
            ntot_w = 0
            for dsid in procGroupConfig[proc]:

                ntot_w += self.GetIntegral_ForDSID(dsid,NormalizedHistosDict  )

            printout += "{:<12}: ".format(proc)
            printout += "{:.3f} ".format(ntot_w)
            printout += "\n"
        return printout

        
    def CalculatePrecision(self,groupedHistos_BG):
        
        #print groupedHistos_BG
        sig =  self.GetIntegral_ForDSID("ee2HHbbbb", groupedHistos_BG)
        #print sig
        bg  = 0
        for proc in groupedHistos_BG.keys():
            if proc == "ee2HHbbbb": continue
            bg  += self.GetIntegral_ForDSID(proc,groupedHistos_BG  )
        precision = self.Precision(sig, bg)
        return precision
    def CalculatePrecision_Sig(self,Sig,groupedHistos_BG):
        
        #print groupedHistos_BG
        sig =  Sig
        #print sig
        bg  = 0
        for proc in groupedHistos_BG.keys():
            if proc == "ee2HHbbbb": continue
            bg  += self.GetIntegral_ForDSID(proc,groupedHistos_BG  )
        precision = self.Precision(sig, bg)
        return precision


    def Precision(self,sig,bg):
        print("Calculating precision as sqrt(S+B)/S")
        #print(sig,bg)
        if sig == 0:
            return 0
        return sqrt(sig+bg)/sig

        
    def GroupHistograms(self, HistosDict, procGroupConfig):
        GroupedHistos = {}
        # get all the histograms with the same name before "_" and the grouped DSIDs after "_" and add them up

        for proc in procGroupConfig.keys():
            #create a list of cloned histos for the first DSID of the process
            hlist = []
            is_firstDSID = True
            for DSID in HistosDict.keys():
                if not DSID in map(str,procGroupConfig[proc]):
                    continue
                if is_firstDSID:
                    for h in HistosDict[DSID]:
                        h.SetName( h.GetName().replace("{}".format(DSID), proc.replace("->","2")) )
                        hlist.append(h)
                else:
                    for hi in hlist:
                        for h in HistosDict[DSID]:
                            if h.GetName().split("_")[0] == hi.GetName().split("_")[0]:
                                hi.Add(h)
                is_firstDSID = False
            GroupedHistos[proc.replace("->","2")] = hlist
                        
        return GroupedHistos