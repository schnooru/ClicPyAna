import json
from ROOT import TH1F, TH2F
import ROOT
from array import array

class HistogramCreator:
    
    def __init__(self, histConfig, tag="",hcolor = 46):
        self.colorDict = {}
        self.colorDict[tag] = hcolor
        self.colorDict["Delphes"] = 46
        self.colorDict["Fullsim"] = 40
        self.colorDict["dst9400"] = 46
        self.colorDict["dst9400"] = 46
        self.colorDict["2558"]=40
        self.colorDict["9400"] = 46
        self.colorDict["6747"] = 8
        self.colorDict["2713"] = 8
        self.colorDict["2441"] = 46
        self.colorDict["Delphes-2439"] = 46
        self.colorDict["2001"] = 40
        self.colorDict["Delphes-1999"] = 46
        self.colorDict[""] = 46

        self.histsdict = {}
        #configFileHist = open(configHistJson).read()
        self.Histconfig = histConfig #configHist = json.loads(configFileHist)

        #if tag == "":
        #    print self.Histconfig
        #    tag = self.Histconfig["Label"]

        for h in self.Histconfig.keys():
            cfg = self.Histconfig[h]
            if cfg["type"] == "TH1F":
                self.histsdict[h] = ROOT.TH1F(cfg["name"]+"_"+tag, tag, cfg["nbins"], cfg["xbinlow"], cfg["xbinup"]  )
                if not self.colorDict[tag]: self.colorDict[tag] = 1
                self.histsdict[h].SetLineColor( self.colorDict[tag])

            elif cfg["type"] == "TH2F":
                #the TH2 need to be binned correctly from the start to avoid too much memory consumption
                #as well as problems with rebinning later (as RebinX, RebinY and Rebin2D does not support bin vectors)
                if not cfg.get("xbinvec") and not cfg.get("ybinvec"):
                    print("WARNING 2D-histograms are potentially not binned correctly (might be heavy on memory, too)")
                    self.histsdict[h] = ROOT.TH2F(cfg["name"]+"_"+tag, tag, cfg["nbinsx"], cfg["xbinlow"], cfg["xbinup"], cfg["nbinsy"], cfg["ybinlow"], cfg["ybinup"]  )
                else:
                    nbinsx = len(cfg["xbinvec"])-1
                    nbinsy = len(cfg["ybinvec"])-1
                    xbinarray, ybinarray = array('d'), array('d')
                    [xbinarray.append(x) for x in cfg["xbinvec"]]
                    [ybinarray.append(y) for y in cfg["ybinvec"]]
                    self.histsdict[h] = ROOT.TH2F(cfg["name"]+"_"+tag, tag, nbinsx, xbinarray, nbinsy, ybinarray )
            
            self.histsdict[h].Sumw2()
            self.histsdict[h].GetYaxis().SetTitle(cfg["ytitle"])
            self.histsdict[h].GetYaxis().SetTitleSize(0.07)
            self.histsdict[h].GetXaxis().SetTitle(cfg["xtitle"] )
            self.histsdict[h].GetXaxis().SetTitleSize(0.07)
            self.histsdict[h].GetYaxis().SetLabelSize(0.07)
            self.histsdict[h].GetXaxis().SetLabelSize(0.07)
            self.histsdict[h].GetXaxis().SetNdivisions(505)
            #self.histsdict[h].SetTitle(cfg["title"] )
        return 
