from ROOT import TLorentzVector
from math import log10
from Event import Event

class WWEvent(Event):
    """Event properties of WW->lnuqq events"""

    y23 = 999
    y34 = 999
    mW = 80
    CME = 3000 #GeV
    
    def __init__(self):
        Event.__init__(self)
        self.dimuonmasslist = []
        self.jetsList2=[]
        self.jetsList2_smeared = []
        self.dijetsmasslist =[]
        self.jetsList3 =[] #needed for y23, y34
        self.jetsList4 =[]


        #always 2 W Jets

        #could be more or less than 2 Hjets:

        return

    def fillCutmap(self):
        self.cutmap["AllJets10GeV"] = self.PassJetsAbove10GeV()
        self.cutmap["CentralJets"] = self.PassJetsCentral()
        self.cutmap["ForwardJets"] = self.PassJetsForward()
        #self.cutmap["TruthVetoWjets"] = self.PassTruthWjetsVeto()
        return

    def PassJetsCentral(self):
        #both jets required to be central to pass thsi cut
        for j in self.jetsList2:
            if abs(j.Eta()) > 0.55:
                return False
        return True
                

    def PassJetsForward(self):
        #both jets required to be forward
        for j in self.jetsList2:
            if abs(j.Eta()) < 0.55:
                return False
        return True

        
    def PassTruthWjetsVeto(self):
        if self.IsWjetsEvent:
            return False
        return True
            

    def PassJetsAbove10GeV(self):
        passcut = True
        for jet in self.jetsList4:
            if jet.Pt() < 10:
                passcut = False
        return passcut
        

        