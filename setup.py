from setuptools import setup

setup(name='clicpyana',
      version='0.1',
      description='Python Analysis for CLIC',
      url='http://gitlab.cern.ch/schnooru/clicpyana',
      author='Ulrike Schnoor',
      author_email='ulrike.schnoor@cern.ch',
      license='MIT',
      packages=['clicpyana'],
      zip_safe=False)