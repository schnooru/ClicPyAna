A general python code useful for data analysis and plotting for simulation studies of measurements in e+e- collisions developed in the context of CLIC analyses.


Ulrike Schnoor <ulrike.schnoor@cern.ch>

# Submodule git instructions
Using the submodule `ClicPyAna` within other packages that provide the data and configurations


Add `ClicPyAna` to the .gitmodules file:
```
[submodule "ClicPyAna"]
        path = ClicPyAn
        url = https://:@gitlab.cern.ch:8443/schnooru/ClicPyAna.git
```

More info on submodules: https://git-scm.com/book/en/v2/Git-Tools-Submodules

## How to add a submodule to a repo
`git submodule add https://:@gitlab.cern.ch:8443/schnooru/ClicPyAna.git`
## How to update it if there is a change in the central repo?
*Fetch and merge locally:*

If you want to check for new work in a submodule, you can go into the directory and run git fetch and git merge the upstream branch to update the local code:
   
```
cd ClicPyAna
git fetch
```   
this updates the origin/master, such that then it is possible to fast-forward by merging:
   
```
git merge origin/master
```
Go back to the main project:
`../`

Then, need to commit in order to update the origin (repo) to the newest commit of the submodule
```
git add ClicPyAna
git commit -m "updated ClicPyAna submodule"
git push -u origin master
```   

*Alternatively*
`git submodule update ClicPyAna`


## How to push changes in the local submodule up to the central submodule's repo
   
Need to tell the submodule a branch, otherwise it is in detached state:
```
cd ClicPyAna
git checkout master
```
then add, commit, and push in the following way:

```
cd /path/to/outer/repo/<submodule>
```
make changes
```
git add <changed.file>
git commit -m "changed file changed.file"
git push origin master # or wherever
```
doing then
```cd  /path/to/outer/repo/
git status
```
says:
`modified: <submodule> (new commits)`

now one needs to tell the outer repo that the current newest commit of <submodule> should be used for the outer repo:
```
cd  /path/to/outer/repo/
git add <submodule>
git commit -m "let's update the <submodule> to the newest commit"
git push origin master
```
If there are other clones of this outer repo, there one has to do
`git submodule update`
(I think inside the <submodule> directory )

